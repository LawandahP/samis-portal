import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

import { 
    studentCreateReducer, 
    studentListReducer,
    studentDetailsReducer,
    studentUpdateReducer 
} from './students/studentReducers'; 

import { 
    streamListReducer,
    streamCreateReducer,
    streamDetailsReducer,
    streamUpdateReducer,
    streamDeleteReducer

} from './streams/streamReducers'
import { 
        testimonialCreateReducer, 
        testimonialDeleteReducer, 
        testimonialListReducer, 
        testimonialUpdateReducer 
    } 
from './testimonials/testimonialReducers';
import { 
    articleCreateReducer, 
    articleDeleteReducer, 
    articleListReducer, 
    articleUpdateReducer 
} from './articles/articleReducers';

import { 
    academicSubjectCreateReducer, 
    academicSubjectDeleteReducer, 
    academicSubjectListReducer, 
    academicSubjectUpdateReducer 
} from './academicSubjects/academicSubjectReducers';

import { 
    kcseSubjectCreateReducer,
    kcseSubjectDeleteReducer, 
    kcseSubjectListReducer, 
    kcseSubjectUpdateReducer 
} from './kcseSubjects/kcseSubjectReducers';

import { 
    contactInfoCreateReducer, 
    contactInfoDeleteReducer, 
    contactInfoGetReducer, 
    contactInfoUpdateReducer 
} from './contactInfo/contactInfoReducers';

import { 
    schoolHistoryCreateReducer, 
    schoolHistoryDeleteReducer, 
    schoolHistoryListReducer, 
    schoolHistoryUpdateReducer 
} from './schoolHistory/schoolHistoryReducers';



const reducers = combineReducers({
    studentCreate: studentCreateReducer,
    studentList: studentListReducer,
    studentUpdate: studentUpdateReducer,
    studentDetails: studentDetailsReducer,
    
    streamList: streamListReducer,
    streamCreate: streamCreateReducer,
    streamDetails: streamDetailsReducer,
    sreamUpdate: streamUpdateReducer,
    streamDelete: streamDeleteReducer,

    testimonialList: testimonialListReducer,
    testimonialCreate: testimonialCreateReducer,
    testimonialUpdate: testimonialUpdateReducer,
    testimonialDelete: testimonialDeleteReducer,

    kcseSubjectList: kcseSubjectListReducer,
    kcseSubjectCreate: kcseSubjectCreateReducer,
    kcseSubjectUpdate: kcseSubjectUpdateReducer,
    kcseSubjectDelete: kcseSubjectDeleteReducer,

    academicSubjectList: academicSubjectListReducer,
    academicSubjectCreate: academicSubjectCreateReducer,
    academicSubjectUpdate: academicSubjectUpdateReducer,
    academicSubjectDelete: academicSubjectDeleteReducer,

    contactInfoGet: contactInfoGetReducer,
    contactInfoCreate: contactInfoCreateReducer,
    contactInfoUpdate: contactInfoUpdateReducer,
    contactInfoDelete: contactInfoDeleteReducer,

    schoolHistoryList: schoolHistoryListReducer,
    schoolHistoryCreate: schoolHistoryCreateReducer,
    schoolHistoryUpdate: schoolHistoryUpdateReducer,
    schoolHistoryDelete: schoolHistoryDeleteReducer,

    articleList: articleListReducer,
    articleCreate: articleCreateReducer,
    articleUpdate: articleUpdateReducer,
    articleDelete: articleDeleteReducer,

})

const initialState = {}


const middleware = [thunk]

const store = createStore(reducers, initialState,
    composeWithDevTools(applyMiddleware(...middleware)));

export default store;