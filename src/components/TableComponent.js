import React, { useState } from 'react';
import { useSelector } from 'react-redux'

import Table from '@mui/material/Table';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import { TableContainer, TableSortLabel } from '@mui/material';
import { makeStyles } from '@mui/styles';


const useStyles = makeStyles(theme => ({
    table: {
        marginTop: theme.spacing(0),
        '& thead th': {
            fontWeight: 600,
            color: theme.pallete,
            backgroundColor: theme.pallete
        },
        '& tbody td': {
            fontWeight: 300,
        },

        '& tbody tr:hover': {
            backgroundColor: '#fffbf2',
            cursor: 'pointer'
        },

    }
}))


export default function TableComponent(records, headCells, search) {
    const classes = useStyles();

    const pages = [5, 10, 15, 20]
    const [ page, setPage ] = useState(0);
    const [ rowsPerPage, setRowsPerPage ] = useState(pages[page])

    const [ order, setOrder ] = useState()
    const [ orderBy, setOrderBy ] = useState()

    const TblContainer = props => (
        
            <TableContainer sx={{ maxHeight: 440 }}>
                <Table stickyHeader aria-label="sticky table" size="small" className={classes.table}>
                    {props.children}
                </Table>
            </TableContainer>
    
    )

    const TblHead = props => {

        const handleSortRequest = cellId => {
            // check whether current sorting is ascending or not
            const isAsc = orderBy === cellId && order === "asc";
            setOrder(isAsc ? 'desc' : 'asc')
            setOrderBy(cellId)
        }
          
        return (
            <TableHead>
                <TableRow>
                    { headCells && headCells.map(headCell =>(
                        <TableCell 
                            key={headCell.id}
                            align={headCell.align}
                            style={{ minWidth: headCell.minWidth}}
                        >
                            { headCell.disableSorting ? headCell.label
                                :<TableSortLabel
                                    active={orderBy === headCell.id}
                                    direction={orderBy === headCell.id ? order : 'asc'}
                                    onClick={() => { handleSortRequest(headCell.id)}}>
                                    {headCell.label}
                                </TableSortLabel>
                            }
                        </TableCell>
                    ))}

                </TableRow>
            </TableHead>
        )
    }

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    }

    const handleChangeRowsPerPage = event => {
        setRowsPerPage(parseInt(event.target.value, 10))
        setPage(0)
    }

    function stableSort(array, comparator) {
        const stabilizedThis = array.map((el, index) => [el, index]);
        stabilizedThis.sort((a, b) => {
            const order = comparator(a[0], b[0]);
            if (order !== 0) return order;
            return a[1] - b[1];
        });
        return stabilizedThis.map((el) => el[0]);
    }

    function getComparator(order, orderBy) {
        return order === 'desc'
            ? (a, b) => descendingComparator(a, b, orderBy)
            : (a, b) => -descendingComparator(a, b, orderBy)
    }

    function descendingComparator(a, b, orderBy) {
        if (b[orderBy] < a[orderBy]) {
            return -1;
        }
        if (b[orderBy] > a[orderBy]) {
            return 1;
        }
        return 0
    }

    // Handles record change
    const recordsAfterPagingAndSorting = () => {
        return stableSort(search.fn(records), getComparator(order, orderBy)).slice(page * rowsPerPage, (page + 1) * rowsPerPage)
    }

    const TblPagination = () => {
        return (
        <TablePagination 
            rowsPerPageOptions={pages}
            component="div"
            count={records.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
        />
    )}

    return {
        TblContainer,
        TblHead,
        TblPagination,
        recordsAfterPagingAndSorting
    }
        
    
}

