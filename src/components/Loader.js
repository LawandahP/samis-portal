import React from 'react'
import { Spinner, Row, Col } from 'react-bootstrap'



function Loader() {
    return (
        <div>
            <Row>
                <Col align="center">

                    <Spinner
                        animation="grow" 
                        role="status"
                        variant="info"
                        style={{
                            margin:"2rem",
                            align: "center"
                        }}
                    />
            
                    <Spinner
                        animation="grow" 
                        role="status"
                        // size="sm"
                        variant="warning"
                        style={{
                            margin:"2rem",
                            align: "center"
                        }}
                    />

                    <Spinner
                        animation="grow" 
                        role="status"
                        variant="danger"
                        style={{
                            margin:"2rem",
                            align: "center"
                        }}
                    />
                    
                </Col>
            </Row>
           
        </div>
        
    )
}

export default Loader
