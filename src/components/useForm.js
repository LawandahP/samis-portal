import React, {useState} from 'react'
import { makeStyles } from '@mui/styles';

const useStyle = makeStyles(theme => ({
    root: {
        '& .MuiFormControl-root' : {
            width: '96%',
            margin: theme.spacing(1)
        }

    }
}))

export function useForm(initialFValues, validateOnChange=false, validate) {

    const [values, setValues] = useState(initialFValues);
    const [errors, setErrors] = useState({});

    const handleResetForm = () => {
        setValues(initialFValues)
        document.getElementById('contained-button-file').value = null
        setErrors({})
    }

    const handleInputChange = e => {
        if (e.target.files && e.target.files[0]) {
            let image_file = e.target.files[0];
            const reader = new FileReader();
            reader.onload = x => {
                setValues({
                    ...values,
                    image_file,
                    image_src: x.target.result
                })
            }
            reader.readAsDataURL(image_file)
        } else {
            const { name, value } = e.target
            setValues({
                ...values,
                [name]:value
            })
            //check whether validateonChange is true
            if(validateOnChange)
                validate({[name]:value})
        }

        
    }

    // const showPreview = e => {
    //     if (e.target.files && e.target.files[0]) {
    //         let image_file = e.target.files[0];
    //         const reader = new FileReader();
    //         reader.onload = x => {
    //             setValues({
    //                 ...values,
    //                 image_file,
    //                 image_src: x.target.result
    //             })
    //         }
    //         reader.readAsDataURL(image_file)
    //     }
    // }

    return {
        values,
        setValues,
        errors,
        setErrors,
        handleInputChange,
        handleResetForm
    }
}




export function Form(props) {
    const classes = useStyle();
    const { children, ...other } = props
    return (
        <form className={classes.root} {...other}>
            {props.children}
        </form>
    )
}
