import { TextField } from '@mui/material'
import React from 'react'

export default function Input(props) {

    const { label, name, value, error=null, onChange, type, size, multiline }  = props;
    return (
        <TextField
            multiline={multiline}
            size={size || "normal"}
            type={type}
            variant="outlined"
            label={label}
            value={value}
            name={name}
            onChange={onChange}
            {...(error && {error:true, helperText:error})}
        />
    )
}
