
   
import React from 'react'
import MobileDatePicker from '@mui/lab/MobileDatePicker';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import Controls from './Controls';
import { TextField } from '@mui/material';
// import DateFnsUtils from "@date-io/date-fns";

export default function DatePicker(props) {

    const { name, label, onChange, size } = props


    const convertToDefEventPara = (name, value) => ({
        target: {
            name, value
        }
    })
    const [value, setValue] = React.useState();
    const handleChange = (newValue) => {
        setValue(newValue);
    };

    return (
        <div>
            <LocalizationProvider dateAdapter={AdapterDateFns}>
                <MobileDatePicker
                    size={size || "small"}
                    label={label}
                    inputFormat="yyyy/MM/dd"
                    name={name}
                    value={value}
                    onChange={handleChange}
                    renderInput={(params) => <TextField {...params} />}
                />
            </LocalizationProvider>
        </div>
        
    )
}