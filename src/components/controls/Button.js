import React from 'react'
import { makeStyles } from '@mui/styles'
import { Button as MuiButton } from "@mui/material";


const useStyles = makeStyles(theme => ({
    root: {
        margin: theme.spacing(5)
    },
    label: {
        textTransform: 'none'
    }
}))

export default function Button(props) {

    const { text, size, color, variant, onClick, ...other } = props
    const classes = useStyles();

    return (
        <MuiButton
            variant={variant || "contained"}
            size={size || "large"}
            color={color || "primary"}
            onClick={onClick}
            {...other}
            className={{ root: classes.root, label: classes.label }}>
            {text}
        </MuiButton>
    )
}