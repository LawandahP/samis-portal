import React, {useState}  from 'react'
import { Toast, Row, Col, ToastContainer } from 'react-bootstrap'


function ToastAlert({bg, type, children}) {
    const [show, setShow] = useState();
  

  
    return (
        <Row>
            <Col xs={6}>
                <ToastContainer className="p-3" position="bottom-end">
                    <Toast bg={bg} onClose={() => setShow(false)} show={show} delay={5000} autohide>
                        {/* <Toast.Header>
                            <img
                                src=""
                                className="me-auto"
                                alt=""
                            />
                            <strong className="me-auto">{type}</strong> */}
                {/* <small>11 mins ago</small> */}
                        {/* </Toast.Header> */}
                        <Toast.Body>{children}</Toast.Body>
                    </Toast>
                </ToastContainer>
            </Col>
        </Row>
    );
}

export default ToastAlert
