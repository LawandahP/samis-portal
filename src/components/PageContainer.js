import React from 'react'
import { Container, Row, Col } from 'react-bootstrap'

function PageContainer({ children}) {
    return (
        <Container fluid className="page-container">
            <Row>
                <Col xs={12} md={12}>
                    {children}
                </Col>
            </Row>
        </Container>
    )
}

export default PageContainer
