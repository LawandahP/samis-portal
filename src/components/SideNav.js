import React from 'react'
import ContactsIcon from '@mui/icons-material/Contacts';

// import { LinkContainer } from 'react-router-bootstrap';
import { Link } from 'react-router-dom';


function SideNav() {
    return (

        <div>
            <nav class="navbar">
                <ul class="navbar-nav">
                    {/* <li class="logo">
                        <Link to="" class="nav-logo-link">
                            <span class="logo-text">SKOOLYCMS</span>
                        </Link>
                    </li> */}

                    <li class="nav-item">
                        <Link to="#" class="nav-link">
                            <i class="fas fa-tachometer-alt fa-primary"></i>
                            <span class="link-text">Dashboard</span>
                        </Link>
                    </li>

                    <li class="nav-item">
                        <Link to="/streams"class="nav-link">
                        <i class="fas fa-stream fa-primary"></i>
                            <span class="link-text">Streams</span>
                        </Link>
                    </li>

                    

                    <li class="nav-item">
                        <Link to="/students" class="nav-link">
                            <i class="fas fa-user-graduate fa-primary"></i>
                            <span class="link-text">Students</span>
                        </Link>
                    </li>

                    <li class="nav-item">
                        <Link to="/testimonials"class="nav-link">
                            <i class="fas fa-chalkboard-teacher fa-primary"></i>
                            <span class="link-text">Testimonials</span>
                        </Link>
                    </li>

                    <li class="nav-item">
                        <Link to="/kcse_subjects" class="nav-link">
                            <i class="fas fa-book-open fa-primary"></i>
                            <span class="link-text">Kcse Subjects</span>
                        </Link>
                    </li>

                    <li class="nav-item">
                        <Link to="/academic_subjects" class="nav-link">
                            <i class="fas fa-book fa-primary"></i>
                            <span class="link-text">Academic Subjects</span>
                        </Link>
                    </li>

                    <li class="nav-item">
                        <Link to="/contact_info" class="nav-link">
                            <i class="fas fa-address-book fa-secondary"></i>
                            <span class="link-text">Contact Info</span>
                        </Link>
                    </li>

                    <li class="nav-item">
                        <Link to="/school_history" class="nav-link">
                            <i class="fas fa-history fa-primary"></i>
                            <span class="link-text">School History</span>
                        </Link>
                    </li>

                    <li class="nav-item">
                        <Link to="/articles" class="nav-link">
                            <i class="far fa-newspaper fa-primary"></i>
                            <span class="link-text">Articles</span>
                        </Link>
                    </li>

                    <li class="nav-item">
                        <Link to="#" class="nav-link">
                            <i class="fas fa-broom fa-primary"></i>
                            <span class="link-text">Report Form</span>
                        </Link>
                    </li>

                    <li class="nav-item">
                        <Link to="#" class="nav-link">
                            <i class="fas fa-broom fa-primary"></i>
                            <span class="link-text">Fee Structure</span>
                        </Link>
                    </li>

                    <li class="nav-item">
                        <Link to="#" class="nav-link">
                            <i class="fas fa-cogs fa-primary"></i>
                            <span class="link-text">Settings</span>
                        </Link>
                    </li>

                </ul>
            </nav>
            
        </div>
        
    )
}

export default SideNav
