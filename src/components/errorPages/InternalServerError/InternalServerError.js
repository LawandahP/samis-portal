import React from 'react'
import './InternalServerError.scss';

function InternalServerError(props) {
    return (
        <div>
             <p className={'internalServer'}>{"500 SERVER ERROR, CONTACT ADMINISTRATOR!!!!"}</p>
        </div>
    )
}

export default InternalServerError
