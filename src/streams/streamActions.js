import axios from 'axios';
import { 
    STREAM_GET_REQUEST,
    STREAM_GET_SUCCESS,
    STREAM_GET_FAIL,

    STREAM_CREATE_REQUEST,
    STREAM_CREATE_SUCCESS,
    STREAM_CREATE_FAIL,
    STREAM_CREATE_RESET,

    STREAM_DETAILS_REQUEST,
    STREAM_DETAILS_SUCCESS,
    STREAM_DETAILS_FAIL,

    STREAM_UPDATE_REQUEST,
    STREAM_UPDATE_SUCCESS,
    STREAM_UPDATE_FAIL,

    STREAM_DELETE_REQUEST,
    STREAM_DELETE_SUCCESS,
    STREAM_DELETE_FAIL,



} from './streamConstants';


export const listStreams = () => async (dispatch) => {
    try {
        dispatch({ type: STREAM_GET_REQUEST })
        const { data } = await axios.get(`/school/streams/v1`)

        dispatch({
            type:STREAM_GET_SUCCESS,
            payload: data
        })
    } catch (error) {
        dispatch({
            type:STREAM_GET_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })
    }
}



export const createStream = (stream) => async (dispatch, getState) => {
    try {
        dispatch({
            type: STREAM_CREATE_REQUEST
        })
        
        const { data } = await axios.post(
            `/school/streams/v1`,
            stream,
        )

        dispatch({
            type: STREAM_CREATE_SUCCESS,
            success: true,
            payload: data
        })

    } catch(error) {
        dispatch({
            type: STREAM_CREATE_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })

    }
}


export const streamDetails = (name) => async (dispatch) => {
    try {
        dispatch({ type: STREAM_DETAILS_REQUEST })
        const { data } = await axios.get(`/school/streams/${name}`) //proxy in package.json "http://127.0.0.1:8000/"

        dispatch({
            type:STREAM_DETAILS_SUCCESS,
            payload: data
        })
    } catch (error) {
        dispatch({
            type:STREAM_DETAILS_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })
    }
}


export const updateStream = (stream) => async (dispatch) => {
    try {
        dispatch({
            type: STREAM_UPDATE_REQUEST
        })
        
        const { data } = await axios.put(
            `/school/streams/v1/${stream.name}`,
            stream,
        )

        dispatch({
            type: STREAM_UPDATE_SUCCESS,
            payload: data
        })

        //update details
        dispatch({
            type: STREAM_DETAILS_SUCCESS,
            payload: data
        })

    } catch(error) {
        dispatch({
            type: STREAM_UPDATE_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })

    }
}


export const deleteStream = (name) => async (dispatch, getState) => {
    try {
        dispatch({
            type: STREAM_DELETE_REQUEST
        })
        
        const { data } = await axios.delete(
            `/school/streams/v1/${name}`,
            //config
        )

        dispatch({
            type: STREAM_DELETE_SUCCESS,
        })

    } catch(error) {
        dispatch({
            type: STREAM_DELETE_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })

    }
}
