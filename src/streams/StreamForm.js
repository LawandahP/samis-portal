import React, {useEffect} from 'react'
import { Grid } from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';

import Loader from '../components/Loader';
import ToastAlert from '../components/ToastAlert';
import {useForm, Form } from '../components/useForm';
import Controls from '../components/controls/Controls';


function StreamForm(props, {history}) {
    const { addOrEdit, recordForEdit, streamId } = props;
    const dispatch = useDispatch();

    const streamCreate = useSelector(state => state.streamCreate)
    const { loading: loadingCreate, error: errorCreate, success: successCreate } = streamCreate

    // const [successMessage, setSuccessMessage] = useState('')
    
    const validate = (fieldValues = values) => {
        let temp = {...errors}
        if('name' in fieldValues)
            temp.name = fieldValues.name ? "" : "Stream Name is Required"
        // if('next_stream' in fieldValues)
        //     temp.next_stream = fieldValues.next_stream ? "" : "Email is Required"
        // temp.phone_no = (\\+?\\d{9,13}).test(values.phone_no) ? "" : "Student Name is Required"
        setErrors({ ...temp })

        // tests whether post array elements passes text implemented by validate() function
        if (fieldValues === values)
            return Object.values(temp).every(x => x === "")
        
    }

    const initialFValues = {
        name: '',
        next_stream: '',
    }

    const { values,  
            setValues,
            errors, 
            setErrors, 
            handleResetForm, 
            handleInputChange } = useForm(initialFValues, true, validate);

    
    
    useEffect(() => {
        if(recordForEdit != null)
            setValues({
                ...recordForEdit
            })
    
    }, [dispatch, history, recordForEdit, successCreate, loadingCreate, errorCreate, setValues, streamId ])


    const submitHandler = (e) => {
        e.preventDefault()
        if (validate()) {
            addOrEdit(values, handleResetForm);
            handleResetForm()
        }
        
            

    // const submitHandler = (e) => {
    //     e.preventDefault()
    //     if (validate()) 
    //         dispatch(registerTenant(
    //             values
    //         ))
    //         handleResetForm()
    }

   
    return (
        
        <div>
            { loadingCreate && <Loader />}
            { errorCreate && <ToastAlert bg="danger">{errorCreate}</ToastAlert>}
                <Form onSubmit={submitHandler}>
                    <Grid container>
                        <Grid item md={6} xs={12}>
                            <Controls.Input
                                error={errors.name}
                                label="Stream Name"
                                value={values.name}
                                name='name'
                                onChange={handleInputChange}
                            />
                        </Grid>

                        <Grid item md={6} xs={12}>
                            <Controls.Input 
                                error={errors.next_stream}
                                label="Next Stream"
                                name='next_stream'
                                value={values.next_stream}
                                onChange={handleInputChange}
                            />
                        </Grid>

                        <div>
                            
                                <Controls.Button
                                    type="submit" 
                                    text="Submit"
                                />
                                <Controls.Button 
                                    onClick={handleResetForm}
                                    text="Reset"
                                    color="secondary"
                                />
                        
                        </div>
                        

                    </Grid>
                </Form>
        </div>
        
            
            
                       
    )
}

export default StreamForm
