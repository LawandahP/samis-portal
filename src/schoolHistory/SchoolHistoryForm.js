import React, {useEffect} from 'react'
import { Grid } from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';

import Loader from '../components/Loader';
import ToastAlert from '../components/ToastAlert';
import {useForm, Form } from '../components/useForm';
import Controls from '../components/controls/Controls';


function SchoolHistoryForm(props, {history}) {
    const { addOrEdit, recordForEdit, schoolHistoryId } = props;
    const dispatch = useDispatch();

    const schoolHistoryCreate = useSelector(state => state.schoolHistoryCreate)
    const { loading: loadingCreate, error: errorCreate, success: successCreate } = schoolHistoryCreate

    // const [successMessage, setSuccessMessage] = useState('')
    
    const validate = (fieldValues = values) => {
        let temp = {...errors}
        if('language' in fieldValues)
            temp.name = fieldValues.name ? "" : "Language is Required"
        if('text' in fieldValues)
            temp.alias = fieldValues.alias ? "" : "Text is Required"
        if('excerpt' in fieldValues)
            temp.category = fieldValues.category ? "" : "Excerpt is Required"
        // temp.phone_no = (\\+?\\d{9,13}).test(values.phone_no) ? "" : "Student Name is Required"
        setErrors({ ...temp })

        // tests whether post array elements passes text implemented by validate() function
        if (fieldValues === values)
            return Object.values(temp).every(x => x === "")
        
    }

    const initialFValues = {
        language: '',
        text: '',
        excerpt: '',
        
    }

    const { values,  
            setValues,
            errors, 
            setErrors, 
            handleResetForm, 
            handleInputChange } = useForm(initialFValues, true, validate);

    
    
    useEffect(() => {
        if(recordForEdit != null)
            setValues({
                ...recordForEdit
            })
    
    }, [dispatch, history, recordForEdit, successCreate, loadingCreate, errorCreate, setValues, schoolHistoryId ])


    const submitHandler = (e) => {
        e.preventDefault()
        if (validate()) {
            addOrEdit(values, handleResetForm);
            handleResetForm()
        }
        
            

    // const submitHandler = (e) => {
    //     e.preventDefault()
    //     if (validate()) 
    //         dispatch(registerTenant(
    //             values
    //         ))
    //         handleResetForm()
    }

   
    return (
        
        <div>
            { loadingCreate && <Loader />}
            { errorCreate && <ToastAlert bg="danger">{errorCreate}</ToastAlert>}
                <Form onSubmit={submitHandler}>
                    <Grid container>
                        <Grid item md={3} xs={12}>
                            <Controls.Input
                                error={errors.language}
                                label="Language"
                                value={values.language}
                                name='language'
                                onChange={handleInputChange}
                            />
                        </Grid>

                        <Grid item md={9} xs={12}>
                            <Controls.Input 
                                error={errors.text}
                                label="Text"
                                name='text'
                                value={values.text}
                                onChange={handleInputChange}
                            />
                        </Grid>

                        <Grid item md={12} xs={12}>
                            <Controls.Input 
                                multiline="true"
                                error={errors.excerpt}
                                label="Excerpt"
                                name='excerpt'
                                value={values.excerpt}
                                onChange={handleInputChange}
                            />
                        </Grid>

                        <div>
                            
                                <Controls.Button
                                    type="submit" 
                                    text="Submit"
                                />
                                <Controls.Button 
                                    onClick={handleResetForm}
                                    text="Reset"
                                    color="secondary"
                                />
                        
                        </div>
                        

                    </Grid>
                </Form>
        </div>
        
            
            
                       
    )
}

export default SchoolHistoryForm
