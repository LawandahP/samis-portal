import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import Loader from '../components/Loader'
import ToastAlert from '../components/ToastAlert'
import TableComponent from '../components/TableComponent'

import { createSchoolHistory, deleteSchoolHistory, listSchoolHistory, updateSchoolHistory } from './schoolHistoryActions'
import { makeStyles } from '@mui/styles'

import EditOutlinedIcon from '@mui/icons-material/EditOutlined';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import Paper from '@mui/material/Paper';
import TableRow from '@mui/material/TableRow';
import Search from '@mui/icons-material/Search'
import Toolbar from '@mui/material/Toolbar';
import InputAdornment from '@mui/material/InputAdornment';
import Fab from '@mui/material/Fab';
import AddIcon from '@mui/icons-material/Add';
import Controls from '../components/controls/Controls'
import SchoolHistoryForm from './SchoolHistoryForm'
import Popup from '../components/Popup'
import ConfirmDialog from '../components/ConfirmDialog'

const useStyles = makeStyles(theme => ({
    searchInput: {
        width : "85%",
    },
    newButton: {
        position: "absolute",
        right: "10px",
        margin: "1rem"
    },
    paper: {
        width: "100%",
        overflow: "hidden",
        marginTop: "1rem",
        marginBottom: "3.5rem"
    }
}))
const headCells = [
    { id: 'language', label: 'Language', minWidth: 50 },
    { id: 'text', label: 'Text', minWidth: 170 },
    { id: 'exerpt', label: 'Exerpt', minWidth: 500 },
    { id: 'actions', label: 'Actions', disableSorting: true },

]

// id(pin):2
// name(pin):"Kiswahili"
// alias(pin):"Kis"
// category(pin):"Compulsory"
// code(pin):102

function SchoolHistoryListScreen() {
    const dispatch = useDispatch();
    const classes = useStyles();

    const [ search, setSearch ] = useState({fn:items => {return items;}})
    const [ recordForEdit, setRecordForEdit ] = useState(null);
    const [ openPopup, setOpenPopup ] = useState(false)

    const schoolHistoryList = useSelector(state => state.schoolHistoryList)
    const { loading, error, schoolHistorys } = schoolHistoryList

    const schoolHistoryCreate = useSelector(state => state.schoolHistoryCreate)
    const { success: successCreate } = schoolHistoryCreate

    const [ confirmDialog, setConfirmDialog ] = useState({isOpen: false, title: '', subTitle: ''})

    const { TblContainer, 
            TblHead, 
            TblPagination, 
            recordsAfterPagingAndSorting 
        } = TableComponent (schoolHistorys, headCells, search)
    
    useEffect(() => {
        dispatch(listSchoolHistory())
    }, [dispatch])

    const handleSearch = (e) => {
        e.preventDefault()
        let target = e.target
        setSearch({
            fn:items => {
                if(target.value === "")
                    return items;
                else
                    return items.filter(x => x.text.toLowerCase().includes(target.value))
            }
        })
    }

    const addOrEdit = (schoolHistory, handleResetForm) => {
        // if (schoolHistory)
        //     dispatch(createSchoolHistory(schoolHistory))
        if (schoolHistory)
            dispatch(updateSchoolHistory(schoolHistory))
        handleResetForm()
        if (successCreate) {
            setOpenPopup(false);
        }
        dispatch(listSchoolHistory())   
        // setNotify({
        //     isOpen:true,
        //     message:'Submitted Successfully',
        //     type:'success'
        // })         
    }

    const editHandler = (schoolHistory) => { 
        setRecordForEdit(schoolHistory)
        setOpenPopup(true)
    }

    const deleteHandler = (id) => {
        setConfirmDialog({
            ...confirmDialog,
            isOpen: false
        })
        dispatch(deleteSchoolHistory(id))  
        dispatch(listSchoolHistory())
    }

    return (
        <div>                   
            { loading
            ? <Loader />
            : error
            ? <ToastAlert bg="danger">{error}</ToastAlert>
            : (
                <Paper className={classes.paper}>
                    <Toolbar>
                        <Controls.Input 
                            className={classes.searchInput}
                            label="Search SchoolHistory"
                            size="small"
                            InputProps = {{
                                startAdornment:(
                                    <InputAdornment position="start">
                                        <Search/>
                                    </InputAdornment>
                                )
                            }}  
                            onChange = { handleSearch } 
                        />

                    <Fab color="primary" size="small" sx={{marginLeft: '.5rem' }} aria-label="add" 
                        onClick = {() => setOpenPopup(true)}
                        >
                        <AddIcon 
                            type="button"/>
                    </Fab>

                   
                    </Toolbar>
                    <TblContainer>
                        <TblHead />

                        <TableBody>
                        {
                            recordsAfterPagingAndSorting() && recordsAfterPagingAndSorting().map(schoolHistory => 
                                (<TableRow key={schoolHistory.id}>
                                    <TableCell>{schoolHistory.language}</TableCell>
                                    <TableCell>{schoolHistory.text}</TableCell>
                                    <TableCell>{schoolHistory.excerpt}</TableCell>
                                    
                                    <TableCell>
                                        <Controls.ActionButton
                                            color="primary">
                                            <EditOutlinedIcon 
                                                onClick={() => editHandler(schoolHistory)}
                                                fontSize="small" />
                                        </Controls.ActionButton>
                                        <Controls.ActionButton 
                                            color="secondary"
                                            onClick={() => {
                                                setConfirmDialog({
                                                    isOpen: true,
                                                    title: "Are you sure you want to delete this schoolHistory?",
                                                    subTitle: "You can't undo this operation",
                                                    onConfirm: () => { deleteHandler(schoolHistory.language)  }
                                                })
                                                
                                            }}>
                                            <DeleteOutlineIcon 
                                                fontSize="small" />
                                            </Controls.ActionButton>
                                        </TableCell>
                                                                
                                </TableRow>)
                        )}
                        </TableBody>
                    </TblContainer>
                    <TblPagination />
                </Paper>
                 
            )}
            <Popup
                openPopup = {openPopup}
                setOpenPopup = {setOpenPopup}
                title="Create School History"
            >
                <SchoolHistoryForm 
                    recordForEdit = {recordForEdit}
                    addOrEdit = {addOrEdit}/>                
            </Popup>  
            <ConfirmDialog 
                confirmDialog={confirmDialog}
                setConfirmDialog={setConfirmDialog} />
            
            </div>
    )
}

export default SchoolHistoryListScreen
