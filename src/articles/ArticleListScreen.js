import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import Loader from '../components/Loader'
import ToastAlert from '../components/ToastAlert'
import TableComponent from '../components/TableComponent'

import { createArticle, deleteArticle, listArticles, updateArticle } from './articleActions'
import { makeStyles } from '@mui/styles'

import EditOutlinedIcon from '@mui/icons-material/EditOutlined';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import Paper from '@mui/material/Paper';
import TableRow from '@mui/material/TableRow';
import Search from '@mui/icons-material/Search'
import Toolbar from '@mui/material/Toolbar';
import InputAdornment from '@mui/material/InputAdornment';
import Fab from '@mui/material/Fab';
import AddIcon from '@mui/icons-material/Add';
import Controls from '../components/controls/Controls'
import ArticleForm from './ArticleForm'
import Popup from '../components/Popup'
import ConfirmDialog from '../components/ConfirmDialog'

const useStyles = makeStyles(theme => ({
    searchInput: {
        width : "85%",
    },
    newButton: {
        position: "absolute",
        right: "10px",
        margin: "1rem"
    },
    paper: {
        width: "100%",
        overflow: "hidden",
        marginTop: "1rem",
        marginBottom: "2rem"
    }
}))
const headCells = [
    { id: 'text', label: 'Text', minWidth: 220 },
    { id: 'excerpt', label: 'Excerpt', minWidth: 300 },
    { id: 'user_id', label: 'Written By', minWidth: 170 },
    { id: 'pen_name', label: 'Alias' },
    { id: 'created_on', label: 'Date created' },
    { id: 'actions', label: 'Actions' },

]

// article_id(pin):1
// text(pin):"Why Choose Skooly"
// excerpt(pin):"Because we don’t cost the earth. We’re not in London, we’re not a big school and we know you want to keep a lid on costs."
// last_updated_on(pin):null
// created_on(pin):null
// user_id(pin):"Githaiga"
// pen_name(pin):"pablo"

function ArticleListScreen() {
    const dispatch = useDispatch();
    const classes = useStyles();

    const [ search, setSearch ] = useState({fn:items => {return items;}})
    const [ recordForEdit, setRecordForEdit ] = useState(null);
    const [ openPopup, setOpenPopup ] = useState(false)

    const articleList = useSelector(state => state.articleList)
    const { loading, error, articles } = articleList

    const articleCreate = useSelector(state => state.articleCreate)
    const { success: successCreate } = articleCreate

    const [ confirmDialog, setConfirmDialog ] = useState({isOpen: false, title: '', subTitle: ''})

    const { TblContainer, 
            TblHead, 
            TblPagination, 
            recordsAfterPagingAndSorting 
        } = TableComponent (articles, headCells, search)
    
    useEffect(() => {
        dispatch(listArticles())
    }, [dispatch])

    const handleSearch = (e) => {
        e.preventDefault()
        let target = e.target
        setSearch({
            fn:items => {
                if(target.value === "")
                    return items;
                else
                    return items.filter(x => x.name.toLowerCase().includes(target.value))
            }
        })
    }

    const addOrEdit = (article, handleResetForm) => {
        if (article)
            dispatch(createArticle(article))
        // if (article)
        //     dispatch(updateArticle(article))
        handleResetForm()
        setOpenPopup(false);
        dispatch(listArticles())
        
           
        // setNotify({
        //     isOpen:true,
        //     message:'Submitted Successfully',
        //     type:'success'
        // })         
    }

    const editHandler = (article) => { 
        setRecordForEdit(article)
        setOpenPopup(true)
    }

    const deleteHandler = (id) => {
        setConfirmDialog({
            ...confirmDialog,
            isOpen: false
        })
        dispatch(deleteArticle(id))  
        dispatch(listArticles())
    }

    return (
        <div>                   
            { loading
            ? <Loader />
            : error
            ? <ToastAlert bg="danger">{error}</ToastAlert>
            : (
                <Paper className={classes.paper}>
                    <Toolbar>
                        <Controls.Input 
                            className={classes.searchInput}
                            label="Search Article"
                            size="small"
                            InputProps = {{
                                startAdornment:(
                                    <InputAdornment position="start">
                                        <Search/>
                                    </InputAdornment>
                                )
                            }}  
                            onChange = { handleSearch } 
                        />

                    <Fab color="primary" size="small" sx={{marginLeft: '.5rem' }} aria-label="add" 
                        onClick = {() => setOpenPopup(true)}
                        >
                        <AddIcon 
                            type="button"/>
                    </Fab>

                   
                    </Toolbar>
                    <TblContainer>
                        <TblHead />

                        <TableBody>
                        {
                            recordsAfterPagingAndSorting() && recordsAfterPagingAndSorting().map(article => 
                                (<TableRow key={article.article_id}>
                                    <TableCell>{article.text}</TableCell>
                                    <TableCell>{article.excerpt}</TableCell>
                                    <TableCell>{article.written_by.user_id}</TableCell>
                                    <TableCell>{article.written_by.pen_name}</TableCell>
                                    <TableCell>{article.created_on}</TableCell>
                                    <TableCell>
                                        <Controls.ActionButton
                                            color="primary">
                                            <EditOutlinedIcon 
                                                onClick={() => editHandler(article)}
                                                fontSize="small" />
                                        </Controls.ActionButton>
                                        <Controls.ActionButton 
                                            color="secondary"
                                            onClick={() => {
                                                setConfirmDialog({
                                                    isOpen: true,
                                                    title: "Are you sure you want to delete this article?",
                                                    subTitle: "You can't undo this operation",
                                                    onConfirm: () => { deleteHandler(article.id)  }
                                                })
                                                
                                            }}>
                                            <DeleteOutlineIcon 
                                                fontSize="small" />
                                            </Controls.ActionButton>
                                        </TableCell>
                                                                
                                </TableRow>)
                        )}
                        </TableBody>
                    </TblContainer>
                    <TblPagination />
                </Paper>
                 
            )}
            <Popup
                openPopup = {openPopup}
                setOpenPopup = {setOpenPopup}
                title="Create Kcse Subject"
            >
                <ArticleForm 
                    recordForEdit = {recordForEdit}
                    addOrEdit = {addOrEdit}/>                
            </Popup>  
            <ConfirmDialog 
                confirmDialog={confirmDialog}
                setConfirmDialog={setConfirmDialog} />
            
            </div>
    )
}

export default ArticleListScreen
