import React, {useEffect} from 'react'
import { Grid } from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';

import Loader from '../components/Loader';
import ToastAlert from '../components/ToastAlert';
import {useForm, Form } from '../components/useForm';
import Controls from '../components/controls/Controls';


function ArticleForm(props, {history}) {
    const { addOrEdit, recordForEdit, contactInfoId } = props;
    const dispatch = useDispatch();

    const contactInfoCreate = useSelector(state => state.contactInfoCreate)
    const { loading: loadingCreate, error: errorCreate, success: successCreate } = contactInfoCreate

    // const [successMessage, setSuccessMessage] = useState('')
    
    const validate = (fieldValues = values) => {
        let temp = {...errors}
        if('physical_address' in fieldValues)
            temp.physical_address = fieldValues.physical_address ? "" : "Physical Address is Required"
        if('postal_address' in fieldValues)
            temp.postal_address = fieldValues.postal_address ? "" : "Postal Address is Required"
        if('primary_phone_no' in fieldValues)
            temp.primary_phone_no = fieldValues.primary_phone_no ? "" : "Primary Phone Number is Required"
        if('email' in fieldValues)
            temp.email = fieldValues.email ? "" : "Email is Required"
        // temp.phone_no = (\\+?\\d{9,13}).test(values.phone_no) ? "" : "Student Name is Required"
        setErrors({ ...temp })

        // tests whether post array elements passes text implemented by validate() function
        if (fieldValues === values)
            return Object.values(temp).every(x => x === "")
        
    }

    const initialFValues = {
        physical_address: '',
        location: {
            latitude: '',
            longitude: '',
            altitude: ''
        },
        postal_address: '',
        country_code: '',
        primary_phone_no: '',
        other_phone_nos: [

        ],
        email: '',
        other_emails: [

        ],
        
    }

    const { values,  
            setValues,
            errors, 
            setErrors, 
            handleResetForm, 
            handleInputChange } = useForm(initialFValues, true, validate);

    
    
    useEffect(() => {
        if(recordForEdit != null)
            setValues({
                ...recordForEdit
            })
    
    }, [dispatch, history, recordForEdit, successCreate, loadingCreate, errorCreate, setValues, contactInfoId ])


    const submitHandler = (e) => {
        e.preventDefault()
        if (validate()) {
            addOrEdit(values, handleResetForm);
            handleResetForm()
        }
        
            

    // const submitHandler = (e) => {
    //     e.preventDefault()
    //     if (validate()) 
    //         dispatch(registerTenant(
    //             values
    //         ))
    //         handleResetForm()
    }

   
    return (
        
        <div>
            { loadingCreate && <Loader />}
            { errorCreate && <ToastAlert bg="danger">{errorCreate}</ToastAlert>}
                <Form onSubmit={submitHandler}>
                    <Grid container>
                        <Grid item md={6} xs={12}>
                            <Controls.Input
                                error={errors.physical_address}
                                label="Physical Address"
                                value={values.physical_address}
                                name='physical_address'
                                onChange={handleInputChange}
                            />
                        </Grid>

                        <Grid item md={6} xs={12}>
                            <Controls.Input
                                error={errors.postal_address}
                                label="Postal Address"
                                value={values.postal_address}
                                name='postal_address'
                                onChange={handleInputChange}
                            />
                        </Grid>
                        

                        <Grid item md={6} xs={12}>
                            <Controls.Input
                                label="Country Code"
                                value={values.country_code}
                                name='country_code'
                                onChange={handleInputChange}
                            />
                        </Grid>

                        <Grid item md={6} xs={12}>
                            <Controls.Input 
                                multiline="true"
                                error={errors.primary_phone_no}
                                label="Phone No. (Primary)"
                                name='primary_phone_no'
                                value={values.primary_phone_no}
                                onChange={handleInputChange}
                            />
                        </Grid>

                        <Grid item md={6} xs={12}>
                            <Controls.Input
                                label="Other Phone Nos"
                                value={values.other_phone_nos}
                                name='other_phone_nos'
                                onChange={handleInputChange}
                            />
                        </Grid>

                        <Grid item md={6} xs={12}>
                            <Controls.Input 
                                error={errors.email}
                                label="Email (Primary)"
                                name='email'
                                value={values.email}
                                onChange={handleInputChange}
                            />
                        </Grid>

                        <Grid item md={6} xs={12}>
                            <Controls.Input
                                multiline="true"
                                label="Other Emails"
                                value={values.other_emails}
                                name='other_emails'
                                onChange={handleInputChange}
                            />
                        </Grid>

                        <Grid item md={12}>
                            Location
                        </Grid>

                        <Grid item md={4} xs={12}>
                            <Controls.Input
                                label="Latitude"
                                value={values.location.latitude}
                                name='latitude'
                                onChange={handleInputChange}
                            />
                        </Grid>

                        <Grid item md={4} xs={12}>
                            <Controls.Input
                                label="Longitude"
                                value={values.location.longitude}
                                name='location'
                                onChange={handleInputChange}
                            />
                        </Grid>

                        <Grid item md={4} xs={12}>
                            <Controls.Input
                                label="Altitude"
                                value={values.location.altitude}
                                name='location'
                                onChange={handleInputChange}
                            />
                        </Grid>

                        <div>
                            <Controls.Button
                                type="submit" 
                                text="Submit"
                            />
                            <Controls.Button 
                                onClick={handleResetForm}
                                text="Reset"
                                color="secondary"
                            />
                        
                        </div>
                        

                    </Grid>
                </Form>
        </div>
        
            
            
                       
    )
}

export default ArticleForm
