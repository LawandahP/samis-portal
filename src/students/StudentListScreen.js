import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import Loader from '../components/Loader'
import ToastAlert from '../components/ToastAlert'
import TableComponent from '../components/TableComponent'

import { listStudents } from './studentActions'
import { makeStyles } from '@mui/styles'

import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import Paper from '@mui/material/Paper';
import TableRow from '@mui/material/TableRow';
import Search from '@mui/icons-material/Search'
import Toolbar from '@mui/material/Toolbar';
import InputAdornment from '@mui/material/InputAdornment';
import Fab from '@mui/material/Fab';
import AddIcon from '@mui/icons-material/Add';
import Controls from '../components/controls/Controls'
import Paginate from '../components/Paginate'

const useStyles = makeStyles(theme => ({
    searchInput: {
        width : "85%",
    },
    newButton: {
        position: "absolute",
        right: "10px",
        margin: "1rem"
    },
    paper: {
        width: "100%",
        overflow: "hidden",
        marginTop: "1rem",
        marginBottom: "2rem"
    }
}))

// const useStyles = makeStyles(theme => ({
//     pageContent: {
//         padding : theme.spacing(1),
//         margin: theme.spacing(2),
//         marginLeft: theme.spacing(2)
//     }
// }))


const headCells = [
    { id: 'student_name', label: 'Student Name', minWidth: 170 },
    { id: 'parent_name', label: 'Parent Name', minWidth: 170 },
    { id: 'phone_no', label: 'Phone No', minWidth: 170 },
    { id: 'email', label: 'Email', minWidth: 170 },
    { id: 'gender', label: 'Gender', minWidth: 170 },
    { id: 'birth_cert_no', label: 'Birth Cert No', minWidth: 170 },
    { id: 'date_of_bith', label: 'D.O.B', minWidth: 170 },
    { id: 'term_admitted', label: 'Term of Admission', minWidth: 170 },
    { id: 'enrollment_type', label: 'Enrollment Type', minWidth: 170 },
    { id: 'admission_date', label: 'Admission Date', minWidth: 170 },
    { id: 'admission_no', label: 'Admission No.', minWidth: 170 },
    { id: 'stream_name', label: 'Stream', minWidth: 170 },
    { id: 'class_of', label: 'Class Of', minWidth: 170 },
    { id: 'house', label: 'House', minWidth: 170 },
    { id: 'upi', label: 'Upi', minWidth: 170 },
    { id: 'KCPE_grade', label: 'Kcpe Grade', minWidth: 100 },
    { id: 'KCPE_marks', label: 'Marks', minWidth: 100 },
    { id: 'KCPE_position', label: 'Position', minWidth: 100 },
    { id: 'KCPE_index_no', label: 'Index No.', minWidth: 100 },
    { id: 'KCPE_meanMarks', label: 'Mean', minWidth: 100 },
    { id: 'KCPE_points', label: 'Points', minWidth: 100 },
    
    
    // {
    //   id: 'density',
    //   label: 'Density',
    //   minWidth: 170,
    //   align: 'right',
    //   format: (value) => value.toFixed(2),
    // },
];



function StudentListScreen({history}) {
    const dispatch = useDispatch();
    const classes = useStyles();

    const [ search, setSearch ] = useState({fn:items => {return items;}})

    const studentList = useSelector(state => state.studentList)
    const { loading, error, students } = studentList

    const { TblContainer, 
            TblHead, 
            TblPagination, 
            recordsAfterPagingAndSorting 
        } = TableComponent (students, headCells, search)

    // const studentDelete = useSelector(state => state.studentDelete)
    // const { loading: loadingDelete, error: errorDelete, success: successDelete } = studentDelete
 

    // const userLogin = useSelector(state => state.userLogin)
    // const { userInfo } = userLogin

    useEffect(() => {
        
        dispatch(listStudents())
        // } else {
        //     history.push("/dashboard")
        // }
        
    }, [dispatch, history])

    const handleSearch = (e) => {
        e.preventDefault()
        let target = e.target
        setSearch({
            fn:items => {
                if(target.value === "")
                    return items;
                else
                    return items.filter(x => x.student_name.toLowerCase().includes(target.value))
            }
        })
    }

    return (
        <div>                   
            { loading
            ? <Loader />
            : error
            ? <ToastAlert bg="danger">{error}</ToastAlert>
            : (
                <Paper classname={classes.paper}>
                    <Toolbar>
                        <Controls.Input 
                            className={classes.searchInput}
                            label="Search Students"
                            size="small"
                            InputProps = {{
                                startAdornment:(
                                    <InputAdornment position="start">
                                        <Search/>
                                    </InputAdornment>
                                )
                            }}  
                            onChange = { handleSearch } 
                        />

                    <Fab color="primary" size="small" sx={{marginLeft: '.5rem' }} aria-label="add" 
                        // onClick = {() => setOpenPopup(true)}
                        >
                        <AddIcon 
                            type="button"/>
                    </Fab>

                   
                    </Toolbar>
                    <TblContainer>
                        <TblHead />

                        <TableBody>
                        {
                            recordsAfterPagingAndSorting() && recordsAfterPagingAndSorting().map(student => 
                                (<TableRow key={student.id}>
                                    <TableCell>{student.student_name}</TableCell>
                                    <TableCell>{student.parent_name}</TableCell>
                                    <TableCell>{student.phone_no}</TableCell>
                                    <TableCell>{student.email}</TableCell>
                                    <TableCell>{student.gender}</TableCell>
                                    <TableCell>{student.birth_cert_no}</TableCell>
                                    <TableCell>{student.date_of_birth}</TableCell>
                                    <TableCell>{student.term_admitted}</TableCell>
                                    <TableCell>{student.enrollment_type}</TableCell>
                                    <TableCell>{student.admission_date}</TableCell>
                                    <TableCell>{student.admission_no}</TableCell>
                                    <TableCell>{student.stream_name}</TableCell>
                                    <TableCell>{student.class_of}</TableCell>
                                    <TableCell>{student.house}</TableCell>
                                    <TableCell>{student.upi}</TableCell>

                                    <TableCell>{student.KCPE_info.KCPE_grade}</TableCell>
                                    <TableCell>{student.KCPE_info.KCPE_marks}</TableCell>
                                    <TableCell>{student.KCPE_info.KCPE_position}</TableCell>
                                    <TableCell>{student.KCPE_info.KCPE_index_no}</TableCell>
                                    <TableCell>{student.KCPE_info.KCPE_meanMarks}</TableCell>
                                    <TableCell>{student.KCPE_info.KCPE_points}</TableCell>
                                    
                                </TableRow>)
                        )}
                        </TableBody>
                    </TblContainer>
                    <TblPagination />
                </Paper>
                 
            )}
            
            </div>
      
    )
}


export default StudentListScreen


