import { 
    STUDENT_CREATE_REQUEST,
    STUDENT_CREATE_SUCCESS,
    STUDENT_CREATE_FAIL,
    STUDENT_CREATE_RESET,

    STUDENT_GET_REQUEST,
    STUDENT_GET_SUCCESS,
    STUDENT_GET_FAIL,
    STUDENT_GET_RESET,

    STUDENT_DETAILS_REQUEST,
    STUDENT_DETAILS_SUCCESS,
    STUDENT_DETAILS_FAIL,
    STUDENT_DETAILS_RESET,

    STUDENT_UPDATE_REQUEST,
    STUDENT_UPDATE_SUCCESS,
    STUDENT_UPDATE_FAIL,
    STUDENT_UPDATE_RESET

} from './studentConstants';


export const studentCreateReducer = (state = {}, action) => {
    switch(action.type) {
        case STUDENT_CREATE_REQUEST:
            return {loading: true}
        
        case STUDENT_CREATE_SUCCESS:
            return {loading: false, success: true, students: action.payload}
        
        case STUDENT_CREATE_FAIL:
            return {loading: false, error: action.payload}
        
        case STUDENT_CREATE_RESET:
            return {}
        default:
            return state
    }
}


export const studentListReducer = (state = { students:[] }, action) =>{
    switch(action.type) {
        case STUDENT_GET_REQUEST:
            return {loading: true, students:[]}
        
        case STUDENT_GET_SUCCESS:
            return {
                        loading: false,
                        students: action.payload.data.items,
                        page: action.payload.data.count,
                        size: action.payload.data.index
                        // page:action.payload.page,
                        // pages:action.payload.pages
                    }
        
        case STUDENT_GET_FAIL:
            return {loading: false, error: action.payload}
        
        case STUDENT_GET_RESET:
            return { users: [] }
        
        default:
            return state
    }
}


export const studentDetailsReducer = (state = { student: {} }, action) => {
    switch(action.type) {
        case STUDENT_DETAILS_REQUEST:
            return {loading: true, ...state}
        
        case STUDENT_DETAILS_SUCCESS:
            return {loading: false, student: action.payload}
        
        case STUDENT_DETAILS_FAIL:
            return {loading: false, error: action.payload}
        
        case STUDENT_DETAILS_RESET:
            return {student: {} }
        
        default:
            return state
    }
}


export const studentUpdateReducer = (state = { student:{} }, action) => {
    switch(action.type) {
        case STUDENT_UPDATE_REQUEST:
            return { loading: true }
        
        case STUDENT_UPDATE_SUCCESS:
            return { loading: false, success: true }
        
        case STUDENT_UPDATE_FAIL:
            return { loading: false, error: action.payload }  
        
        case STUDENT_UPDATE_RESET:
            return { student:{} }
             
        default:
            return state
    }
}