import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { Paper } from '@mui/material';
import { makeStyles } from '@mui/styles'

import { Link } from 'react-router-dom'
import { Grid } from '@mui/material';
import { useForm, Form } from '../components/useForm';
import Notification from '../components/Notification'
import { createStudent } from './studentActions';

import Loader from '../components/Loader'
import Controls from '../components/controls/Controls';
import ToastAlert from '../components/ToastAlert';

const useStyles = makeStyles(theme => ({
    pageContent: {
        padding : theme.spacing(1),
        margin: theme.spacing(2),
        marginLeft: theme.spacing(2)
    }
}))

function StudentCreateScreen({history, location}) {

    const classes = useStyles();

    const genderItems = [
        { id: 'male', title: 'Male' },
        { id: 'female', title: 'Female' },
        { id: 'other', title: 'Other' },
    ]

    const enrollmentType = [
        { id: 'day', title: 'DAY' },
        { id: 'boarding', title: 'BOARDING' },
        { id: 'part time', title: 'PART TIME' },
        { id: 'other', title: 'OTHER' },
    ]

    const validate = (fieldValues = values) => {
        let temp = {...errors}
        if('student_name' in fieldValues)
            temp.student_name = fieldValues.student_name ? "" : "Student Name is Required"
        if('parent_name' in fieldValues)
            temp.parent_name = fieldValues.parent_name ? "" : "Parent Name is Required"
        // temp.phone_no = (\\+?\\d{9,13}).test(values.phone_no) ? "" : "Student Name is Required"
        setErrors({
            ...temp
        })

        // tests whether post array elements passes text implemented by validate() function
        if (fieldValues === values)
            return Object.values(temp).every(x => x === "")
        
    }

    const initialFValues = {
        id: 0,
        student_name: '',
        parent_name: '',
        email: '',
        phone_no: '',
        birth_cert_no: '',
        gender: 'male',
        date_of_birth: '',
        term_admitted: '2016/3',
        enrollment_type: 'boarding',
        house: '',
        upi: '',
        stream_name: '',
        admission_date: '',
        admission_no: '',
        class_of: '',
        

        KCPE_grade: 'B',
        KCPE_marks: '',
        KCPE_position: '',
        KCPE_meanMarks: '',
        KCPE_points: '',
        KCPE_index_no: ''

    }
    const redirect = location.search ? location.search.split('='[1]) : '/shop'

    const dispatch = useDispatch();

    const studentCreate = useSelector(state => state.studentCreate)
    const { error, loading, success } = studentCreate

    
    const { values, 
            setValues, 
            errors, 
            setErrors, 
            handleInputChange,
            handleResetForm } = useForm(initialFValues, true, validate);

    

    // creates student
    const submitHandler = (e) => {
        e.preventDefault()
        if (validate())
            window.alert("Submitted...")
        dispatch(createStudent(
            values
        ))

    }

    useEffect(() => {
        // if(!userInfo || userInfo.IsAdmin) {
        //     history.push(redirect)
        // } else {
        if(success) {
            history.push('/products')
        }
        
    }, [history, redirect, success])


    return (
        
        
        <div>
                        
            {/* <Link to='#'>
                <i class="fas fa-arrow-left"></i>
            </Link> */}

            
            
            
            

            {loading && <Loader /> }
            {error && <ToastAlert bg='danger'>{error}</ToastAlert>}
    
                <Paper className={classes.pageContent}>
                    <Form onSubmit={submitHandler}>
                        <Grid container>
                            <Grid item md={6} xs={12}>
                                <Controls.Input
                                    label="Student Name"
                                    value={values.student_name}
                                    name='student_name'
                                    onChange={handleInputChange}
                                    error={errors.student_name}
                                />
                            </Grid>
                            <Grid item md={6} xs={12}>
                                <Controls.Input
                                    label="Parent Name"
                                    value={values.parent_name}
                                    name='parent_name'
                                    onChange={handleInputChange}
                                    error={errors.parent_name}
                                />
                            </Grid>

                            <Grid item md={2} xs={12}>
                                <Controls.Select
                                    label="Gender"
                                    value={values.gender}
                                    name='gender'
                                    onChange={handleInputChange}
                                    options={genderItems}
                                />
                            </Grid>
                            
                            <Grid item md={4} xs={12}>
                                <Controls.Input
                                    label="Phone No"
                                    value={values.phone_no}
                                    name='phone_no'
                                    onChange={handleInputChange}
                                />
                            </Grid>

                            <Grid item md={6} xs={12}>
                                <Controls.Input
                                    type="email"
                                    label="Email"
                                    value={values.email}
                                    name='email'
                                    onChange={handleInputChange}
                                />
                            </Grid>

                            <Grid item md={2} xs={12}>
                                <Controls.DatePicker
                                    label="D.O.B"
                                    value={values.date_of_birth}
                                    name='date_of_birth'
                                    onChange={handleInputChange}
                                />
                            </Grid>

                            <Grid item md={4} xs={12}>
                                <Controls.Input
                                    label="Birth Cert No"
                                    value={values.birth_cert_no}
                                    name='birth_cert_no'
                                    onChange={handleInputChange}
                                />
                            </Grid>

                            <Grid item md={2} xs={12}>
                                <Controls.Input
                                    label="Term Admitted"
                                    value={values.term_admitted}
                                    name='term_admitted'
                                    onChange={handleInputChange}
                                />
                            </Grid>

                            <Grid item md={2} xs={12}>
                                <Controls.Select
                                    label="Enrollment Type"
                                    value={values.enrollment_type}
                                    name='enrollment_type'
                                    onChange={handleInputChange}
                                    options={enrollmentType}
                                />
                            </Grid>

                            <Grid item md={2} xs={12}>
                                <Controls.Input
                                    label="Admission No"
                                    value={values.admission_no}
                                    name='admission_no'
                                    onChange={handleInputChange}
                                />
                            </Grid>
                            

                            <Grid item md={2} xs={12}>
                                <Controls.DatePicker
                                    label="Admission Date"
                                    value={values.admission_date}
                                    name='admission_date'
                                    onChange={handleInputChange}
                                />
                            </Grid>
                            

                            <Grid item md={2} xs={12}>
                                <Controls.Input
                                    label="Stream Name"
                                    value={values.stream_name}
                                    name='stream_name'
                                    onChange={handleInputChange}
                                />
                            </Grid>

                            <Grid item md={2} xs={12}>
                                <Controls.Input
                                    type="number"
                                    label="Class Of"
                                    value={values.class_of}
                                    name='class_of'
                                    onChange={handleInputChange}
                                />
                            </Grid>

                            <Grid item md={3} xs={12}>
                                <Controls.Input
                                    label="House"
                                    value={values.house}
                                    name='house'
                                    onChange={handleInputChange}
                                />
                            </Grid>
                            <Grid item md={3} xs={12}>
                                <Controls.Input
                                    label="Upi"
                                    value={values.upi}
                                    name='upi'
                                    onChange={handleInputChange}
                                />
                            </Grid>

                            <Grid item md={12} xs={12}>
                                <p>KCPE Info</p>
                            </Grid>

                            <Grid item md={6} xs={12}>
                                <Controls.Input
                                    type="number"
                                    label="Marks"
                                    value={values.KCPE_marks}
                                    name='KCPE_marks'
                                    onChange={handleInputChange}
                                />
                            </Grid>
                            <Grid item md={6} xs={12}>
                                <Controls.Input
                                    type="number"
                                    label="Mean marks"
                                    value={values.KCPE_meanMarks}
                                    name='KCPE_meanMarks'
                                    onChange={handleInputChange}
                                />
                            </Grid>

                            <Grid item md={2} xs={12}>
                                <Controls.Input
                                    label="Grade"
                                    value={values.KCPE_grade}
                                    name='KCPE_grade'
                                    onChange={handleInputChange}
                                />
                            </Grid>
                            <Grid item md={4} xs={12}>
                                <Controls.Input
                                    type="number"
                                    label="Points"
                                    value={values.KCPE_points}
                                    name='KCPE_points'
                                    onChange={handleInputChange}
                                />
                            </Grid>

                            <Grid item md={4} xs={12}>
                                <Controls.Input
                                    type="number"
                                    label="Index No"
                                    value={values.KCPE_index_no}
                                    name='KCPE_index_no'
                                    onChange={handleInputChange}
                                />
                            </Grid>

                            
                            <Grid item md={2} xs={12}>
                                <Controls.Input
                                    type="number"
                                    label="Position"
                                    value={values.KCPE_position}
                                    name='KCPE_position'
                                    onChange={handleInputChange}
                                />
                            </Grid>

                            <div>
                                <Controls.Button
                                    type="submit" 
                                    text="Submit"
                                />
                                <Controls.Button 
                                    text="Reset"
                                    onClick={handleResetForm}
                                    color="secondary"
                                />
                            </div>


                            
                            
                        </Grid>
                    </Form>
                </Paper>
                
                

                
                 
    

    </div>
        
    )
}    
        
export default StudentCreateScreen
