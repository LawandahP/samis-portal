import axios from 'axios';
import { 
    STUDENT_CREATE_REQUEST,
    STUDENT_CREATE_SUCCESS,
    STUDENT_CREATE_FAIL,
    // STUDENT_CREATE_RESET
    STUDENT_GET_REQUEST,
    STUDENT_GET_SUCCESS,
    STUDENT_GET_FAIL,

    STUDENT_DETAILS_REQUEST,
    STUDENT_DETAILS_SUCCESS,
    STUDENT_DETAILS_FAIL,

    STUDENT_UPDATE_REQUEST,
    STUDENT_UPDATE_SUCCESS,
    STUDENT_UPDATE_FAIL,
    // STUDENT_UPDATE_RESET
    

} from './studentConstants';  




export const createStudent = (student) => async (dispatch, getState) => {
    try {
        dispatch({
            type: STUDENT_CREATE_REQUEST
        })
        
        const { data } = await axios.post(
            "http://backend.gathathi.iamgusto.com/school/students/v1",
            student,
        )

        dispatch({
            type: STUDENT_CREATE_SUCCESS,
            payload: data
        })

    } catch(error) {
        dispatch({
            type: STUDENT_CREATE_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })

    }
}


export const listStudents = (page) => async (dispatch) => {
    try {
        dispatch({ type: STUDENT_GET_REQUEST })
        const { data } = await axios.get(`http://backend.gathathi.iamgusto.com/school/students/v1?${page}&size=20000`)

        dispatch({
            type:STUDENT_GET_SUCCESS,
            payload: data
        })
    } catch (error) {
        dispatch({
            type:STUDENT_GET_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })
    }
}



export const getStudentsDetails = (admission_no) => async (dispatch, getState) => {
    try {
        dispatch({
            type: STUDENT_DETAILS_REQUEST
        })
        
        const { data } = await axios.get(
            `/school/students/v1/${admission_no}`,
            // config
        )

        dispatch({
            type: STUDENT_DETAILS_SUCCESS,
            payload: data
        })


    } catch(error) {
        dispatch({
            type: STUDENT_DETAILS_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })

    }
}

export const updateStudent = (student) => async (dispatch, getState) => {
    try {
        dispatch({
            type: STUDENT_UPDATE_REQUEST
        })

        //get current user info
        // const {
        //     userLogin: { userInfo },
        // } = getState()
        
        //send request with token
        // const config = {
        //     headers: {
        //         'Content-type':'application/json',
        //         Authorization: `Bearer ${userInfo.token}`
        //     }
        // }
        
        const { data } = await axios.put(
            `/school/students/v1/${student.admission_no}`,
            student,
            // config
        )

        dispatch({
            type: STUDENT_UPDATE_SUCCESS,
        }) 
        dispatch({
            type: STUDENT_DETAILS_SUCCESS,
            payload: data
        })

    } catch(error) {
        dispatch({
            type: STUDENT_UPDATE_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })

    }
}
