import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import Loader from '../components/Loader'
import ToastAlert from '../components/ToastAlert'
import TableComponent from '../components/TableComponent'

import { createKcseSubject, deleteKcseSubject, listKcseSubjects, updateKcseSubject } from './kcseSubjectActions'
import { makeStyles } from '@mui/styles'

import EditOutlinedIcon from '@mui/icons-material/EditOutlined';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import Paper from '@mui/material/Paper';
import TableRow from '@mui/material/TableRow';
// import Search from '@mui/icons-material/Search'
import Toolbar from '@mui/material/Toolbar';
import InputAdornment from '@mui/material/InputAdornment';
import Fab from '@mui/material/Fab';
import AddIcon from '@mui/icons-material/Add';
import Controls from '../components/controls/Controls'
import KcseSubjectForm from './KcseSubjectForm'
import Popup from '../components/Popup'
import ConfirmDialog from '../components/ConfirmDialog';

import SearchIcon from '@mui/icons-material/Search';
import { styled, alpha } from '@mui/material/styles';
import InputBase from '@mui/material/InputBase';

const Search = styled('div')(({ theme }) => ({
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: alpha(theme.palette.common.black, 0.005),
    '&:hover': {
      backgroundColor: alpha(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing(10),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
    //   marginRight: theme.spacing(0),
      width: 'auto',
    },
  }));
  
  const SearchIconWrapper = styled('div')(({ theme }) => ({
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  }));
  
  const StyledInputBase = styled(InputBase)(({ theme }) => ({
    color: 'inherit',
    '& .MuiInputBase-input': {
      padding: theme.spacing(1, 1, 1, 0),
      // vertical padding + font size from searchIcon
      paddingLeft: `calc(1em + ${theme.spacing(4)})`,
      transition: theme.transitions.create('width'),
      width: '100%',
      [theme.breakpoints.up('md')]: {
        width: '20ch',
      },
    },
  }));

const useStyles = makeStyles(theme => ({
    searchInput: {
        width : "85%",
    },
    newButton: {
        position: "absolute",
        right: "10px",
        margin: "1rem"
    },
    paper: {
        width: "100%",
        overflow: "hidden",
        marginTop: "1rem",
        marginBottom: "2rem"
    }
}))
const headCells = [
    { id: 'name', label: 'Name', minWidth: 120 },
    { id: 'alias', label: 'Alias', minWidth: 100 },
    { id: 'category', label: 'Category', minWidth: 100 },
    { id: 'code', label: 'Code' },
    { id: 'actions', label: 'Actions' },

]


function KcseSubjectListScreen() {
    const dispatch = useDispatch();
    const classes = useStyles();

    const [ search, setSearch ] = useState({fn:items => {return items;}})
    const [ recordForEdit, setRecordForEdit ] = useState(null);
    const [ openPopup, setOpenPopup ] = useState(false)

    const kcseSubjectList = useSelector(state => state.kcseSubjectList)
    const { loading, error, kcseSubjects } = kcseSubjectList

    const kcseSubjectCreate = useSelector(state => state.kcseSubjectCreate)
    const { success: successCreate } = kcseSubjectCreate

    const [ confirmDialog, setConfirmDialog ] = useState({isOpen: false, title: '', subTitle: ''})

    const { TblContainer, 
            TblHead, 
            TblPagination, 
            recordsAfterPagingAndSorting 
        } = TableComponent (kcseSubjects, headCells, search)
    
    useEffect(() => {
        dispatch(listKcseSubjects())
    }, [dispatch])

    const handleSearch = (e) => {
        e.preventDefault()
        let target = e.target
        setSearch({
            fn:items => {
                if(target.value === "")
                    return items;
                else
                    return items.filter(x => x.name.toLowerCase().includes(target.value))
            }
        })
    }

    const addOrEdit = (kcseSubject, handleResetForm) => {
        if (kcseSubject.id === 0) {
            dispatch(createKcseSubject(kcseSubject))
            dispatch(listKcseSubjects())
            setOpenPopup(false);

        } else {
            dispatch(updateKcseSubject(kcseSubject))
            dispatch(listKcseSubjects())
            setOpenPopup(false);
        } 
        handleResetForm()
        
        
        
           
        // setNotify({
        //     isOpen:true,
        //     message:'Submitted Successfully',
        //     type:'success'
        // })         
    }

    const editHandler = (kcseSubject) => { 
        setRecordForEdit(kcseSubject)
        setOpenPopup(true)
    }

    const deleteHandler = (id) => {
        setConfirmDialog({
            ...confirmDialog,
            isOpen: false
        })
        dispatch(deleteKcseSubject(id))  
        dispatch(listKcseSubjects())
    }

    return (
        <div>                   
            { loading
            ? <Loader />
            : error
            ? <ToastAlert bg="danger">{error}</ToastAlert>
            : (
                <Paper className={classes.paper}>
                    <Toolbar>
                        {/* <Controls.Input 
                            className={classes.searchInput}
                            label="Search KcseSubject"
                            size="small"
                            inputProps = {{
                                startAdornment:(
                                    <InputAdornment position="start">
                                        <Search/>
                                    </InputAdornment>
                                )
                            }}  
                            onChange = { handleSearch } 
                        /> */}
                    
                    <Search>
                        <SearchIconWrapper>
                        <SearchIcon />
                        </SearchIconWrapper>
                        <StyledInputBase
                        placeholder="Search…"
                        inputProps={{ 'aria-label': 'search' }}
                        onChange={ handleSearch }
                        />
                    </Search>

                    <Fab color="primary" size="small" sx={{marginLeft: '.5rem' }} aria-label="add" 
                        onClick = {() => setOpenPopup(true)}
                        >
                        <AddIcon 
                            type="button"/>
                    </Fab>

                   
                    </Toolbar>
                    <TblContainer>
                        <TblHead />

                        <TableBody>
                        {
                            recordsAfterPagingAndSorting() && recordsAfterPagingAndSorting().map(kcseSubject => 
                                (<TableRow key={kcseSubject.id}>
                                    <TableCell>{kcseSubject.name}</TableCell>
                                    <TableCell>{kcseSubject.alias}</TableCell>
                                    <TableCell>{kcseSubject.category}</TableCell>
                                    <TableCell>{kcseSubject.code}</TableCell>
                                    <TableCell>
                                        <Controls.ActionButton
                                            color="primary">
                                            <EditOutlinedIcon 
                                                onClick={() => editHandler(kcseSubject)}
                                                fontSize="small" />
                                        </Controls.ActionButton>
                                        <Controls.ActionButton 
                                            color="secondary"
                                            onClick={() => {
                                                setConfirmDialog({
                                                    isOpen: true,
                                                    title: "Are you sure you want to delete this kcseSubject?",
                                                    subTitle: "You can't undo this operation",
                                                    onConfirm: () => { deleteHandler(kcseSubject.id)  }
                                                })
                                                
                                            }}>
                                            <DeleteOutlineIcon 
                                                fontSize="small" />
                                            </Controls.ActionButton>
                                        </TableCell>
                                                                
                                </TableRow>)
                        )}
                        </TableBody>
                    </TblContainer>
                    <TblPagination />
                </Paper>
                 
            )}
            <Popup
                openPopup = {openPopup}
                setOpenPopup = {setOpenPopup}
                title="Create Kcse Subject"
            >
                <KcseSubjectForm 
                    recordForEdit = {recordForEdit}
                    addOrEdit = {addOrEdit}/>                
            </Popup>  
            <ConfirmDialog 
                confirmDialog={confirmDialog}
                setConfirmDialog={setConfirmDialog} />
            
            </div>
    )
}

export default KcseSubjectListScreen
