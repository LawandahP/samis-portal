import React, {useEffect} from 'react'
import { Grid } from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';

import Loader from '../components/Loader';
import ToastAlert from '../components/ToastAlert';
import {useForm, Form } from '../components/useForm';
import Controls from '../components/controls/Controls';


function AcademicSubjectForm(props, {history}) {
    const { addOrEdit, recordForEdit, academicSubjectId } = props;
    const dispatch = useDispatch();

    const academicSubjectCreate = useSelector(state => state.academicSubjectCreate)
    const { loading: loadingCreate, error: errorCreate, success: successCreate } = academicSubjectCreate

    // const [successMessage, setSuccessMessage] = useState('')
    
    const validate = (fieldValues = values) => {
        let temp = {...errors}
        if('name' in fieldValues)
            temp.name = fieldValues.name ? "" : "Subject Name is Required"
        if('alias' in fieldValues)
            temp.alias = fieldValues.alias ? "" : "Alias is Required"
        if('category' in fieldValues)
            temp.category = fieldValues.category ? "" : "Category is Required"
        if('code' in fieldValues)
            temp.code = fieldValues.code ? "" : "Subject code is Required"
        // temp.phone_no = (\\+?\\d{9,13}).test(values.phone_no) ? "" : "Student Name is Required"
        setErrors({ ...temp })

        // tests whether post array elements passes text implemented by validate() function
        if (fieldValues === values)
            return Object.values(temp).every(x => x === "")
        
    }

    const initialFValues = {
        name: '',
        alias: '',
        category: '',
        code: '',
    }

    const { values,  
            setValues,
            errors, 
            setErrors, 
            handleResetForm, 
            handleInputChange } = useForm(initialFValues, true, validate);

    
    
    useEffect(() => {
        if(recordForEdit != null)
            setValues({
                ...recordForEdit
            })
    
    }, [dispatch, history, recordForEdit, successCreate, loadingCreate, errorCreate, setValues, academicSubjectId ])


    const submitHandler = (e) => {
        e.preventDefault()
        if (validate()) {
            addOrEdit(values, handleResetForm);
            handleResetForm()
        }
        
            

    // const submitHandler = (e) => {
    //     e.preventDefault()
    //     if (validate()) 
    //         dispatch(registerTenant(
    //             values
    //         ))
    //         handleResetForm()
    }

   
    return (
        
        <div>
            { loadingCreate && <Loader />}
            { errorCreate && <ToastAlert bg="danger">{errorCreate}</ToastAlert>}
                <Form onSubmit={submitHandler}>
                    <Grid container>
                        <Grid item md={6} xs={12}>
                            <Controls.Input
                                error={errors.name}
                                label="Name"
                                value={values.name}
                                name='name'
                                onChange={handleInputChange}
                            />
                        </Grid>

                        <Grid item md={6} xs={12}>
                            <Controls.Input 
                                error={errors.alias}
                                label="Alias"
                                name='alias'
                                value={values.alias}
                                onChange={handleInputChange}
                            />
                        </Grid>

                        <Grid item md={6} xs={12}>
                            <Controls.Input 
                                error={errors.category}
                                label="Category"
                                name='category'
                                value={values.category}
                                onChange={handleInputChange}
                            />
                        </Grid>

                        <Grid item md={6} xs={12}>
                            <Controls.Input 
                                type="number"
                                error={errors.code}
                                label="Code"
                                name='code'
                                value={values.code}
                                onChange={handleInputChange}
                            />
                        </Grid>

                        <div>
                            
                                <Controls.Button
                                    type="submit" 
                                    text="Submit"
                                />
                                <Controls.Button 
                                    onClick={handleResetForm}
                                    text="Reset"
                                    color="secondary"
                                />
                        
                        </div>
                        

                    </Grid>
                </Form>
        </div>
        
            
            
                       
    )
}

export default AcademicSubjectForm
