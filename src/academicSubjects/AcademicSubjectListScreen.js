import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import Loader from '../components/Loader'
import ToastAlert from '../components/ToastAlert'
import TableComponent from '../components/TableComponent'

import { createAcademicSubject, deleteAcademicSubject, listAcademicSubjects, updateAcademicSubject } from './academicSubjectActions'
import { makeStyles } from '@mui/styles'

import EditOutlinedIcon from '@mui/icons-material/EditOutlined';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import Paper from '@mui/material/Paper';
import TableRow from '@mui/material/TableRow';
import Search from '@mui/icons-material/Search'
import Toolbar from '@mui/material/Toolbar';
import InputAdornment from '@mui/material/InputAdornment';
import Fab from '@mui/material/Fab';
import AddIcon from '@mui/icons-material/Add';
import Controls from '../components/controls/Controls'
import AcademicSubjectForm from './AcademicSubjectForm'
import Popup from '../components/Popup'
import ConfirmDialog from '../components/ConfirmDialog'

const useStyles = makeStyles(theme => ({
    searchInput: {
        width : "85%",
    },
    newButton: {
        position: "absolute",
        right: "10px",
        margin: "1rem"
    },
    paper: {
        width: "100%",
        overflow: "hidden",
        marginTop: "1rem",
        marginBottom: "3.5rem"
    }
}))
const headCells = [
    { id: 'name', label: 'Name', minWidth: 100 },
    { id: 'alias', label: 'Alias', minWidth: 70 },
    { id: 'category', label: 'Category', minWidth: 100 },
    { id: 'code', label: 'Code', minWidth: 70 },
    { id: 'actions', label: 'Actions', disableSorting: true },

]

// id(pin):2
// name(pin):"Kiswahili"
// alias(pin):"Kis"
// category(pin):"Compulsory"
// code(pin):102

function AcademicSubjectListScreen() {
    const dispatch = useDispatch();
    const classes = useStyles();

    const [ search, setSearch ] = useState({fn:items => {return items;}})
    const [ recordForEdit, setRecordForEdit ] = useState(null);
    const [ openPopup, setOpenPopup ] = useState(false)

    const academicSubjectList = useSelector(state => state.academicSubjectList)
    const { loading, error, academicSubjects } = academicSubjectList

    const academicSubjectCreate = useSelector(state => state.academicSubjectCreate)
    const { success: successCreate } = academicSubjectCreate

    const [ confirmDialog, setConfirmDialog ] = useState({isOpen: false, title: '', subTitle: ''})

    const { TblContainer, 
            TblHead, 
            TblPagination, 
            recordsAfterPagingAndSorting 
        } = TableComponent (academicSubjects, headCells, search)
    
    useEffect(() => {
        dispatch(listAcademicSubjects())
    }, [dispatch])

    const handleSearch = (e) => {
        e.preventDefault()
        let target = e.target
        setSearch({
            fn:items => {
                if(target.value === "")
                    return items;
                else
                    return items.filter(x => x.name.toLowerCase().includes(target.value))
            }
        })
    }

    const addOrEdit = (academicSubject, handleResetForm) => {
        // if (academicSubject)
        //     dispatch(createAcademicSubject(academicSubject))
        if (academicSubject)
            dispatch(updateAcademicSubject(academicSubject))
        handleResetForm()
        if (successCreate) {
            setOpenPopup(false);
        }
        dispatch(listAcademicSubjects())   
        // setNotify({
        //     isOpen:true,
        //     message:'Submitted Successfully',
        //     type:'success'
        // })         
    }

    const editHandler = (academicSubject) => { 
        setRecordForEdit(academicSubject)
        setOpenPopup(true)
    }

    const deleteHandler = (id) => {
        setConfirmDialog({
            ...confirmDialog,
            isOpen: false
        })
        dispatch(deleteAcademicSubject(id))  
        dispatch(listAcademicSubjects())
    }

    return (
        <div>                   
            { loading
            ? <Loader />
            : error
            ? <ToastAlert bg="danger">{error}</ToastAlert>
            : (
                <Paper className={classes.paper}>
                    <Toolbar>
                        <Controls.Input 
                            className={classes.searchInput}
                            label="Search AcademicSubject"
                            size="small"
                            InputProps = {{
                                startAdornment:(
                                    <InputAdornment position="start">
                                        <Search/>
                                    </InputAdornment>
                                )
                            }}  
                            onChange = { handleSearch } 
                        />

                    <Fab color="primary" size="small" sx={{marginLeft: '.5rem' }} aria-label="add" 
                        onClick = {() => setOpenPopup(true)}
                        >
                        <AddIcon 
                            type="button"/>
                    </Fab>

                   
                    </Toolbar>
                    <TblContainer>
                        <TblHead />

                        <TableBody>
                        {
                            recordsAfterPagingAndSorting() && recordsAfterPagingAndSorting().map(academicSubject => 
                                (<TableRow key={academicSubject.id}>
                                    <TableCell>{academicSubject.name}</TableCell>
                                    <TableCell>{academicSubject.alias}</TableCell>
                                    <TableCell>{academicSubject.category}</TableCell>
                                    <TableCell>{academicSubject.code}</TableCell>
                                    <TableCell>
                                        <Controls.ActionButton
                                            color="primary">
                                            <EditOutlinedIcon 
                                                onClick={() => editHandler(academicSubject)}
                                                fontSize="small" />
                                        </Controls.ActionButton>
                                        <Controls.ActionButton 
                                            color="secondary"
                                            onClick={() => {
                                                setConfirmDialog({
                                                    isOpen: true,
                                                    title: "Are you sure you want to delete this academicSubject?",
                                                    subTitle: "You can't undo this operation",
                                                    onConfirm: () => { deleteHandler(academicSubject.id)  }
                                                })
                                                
                                            }}>
                                            <DeleteOutlineIcon 
                                                fontSize="small" />
                                            </Controls.ActionButton>
                                        </TableCell>
                                                                
                                </TableRow>)
                        )}
                        </TableBody>
                    </TblContainer>
                    <TblPagination />
                </Paper>
                 
            )}
            <Popup
                openPopup = {openPopup}
                setOpenPopup = {setOpenPopup}
                title="Create Academic Subject"
            >
                <AcademicSubjectForm 
                    recordForEdit = {recordForEdit}
                    addOrEdit = {addOrEdit}/>                
            </Popup>  
            <ConfirmDialog 
                confirmDialog={confirmDialog}
                setConfirmDialog={setConfirmDialog} />
            
            </div>
    )
}

export default AcademicSubjectListScreen
