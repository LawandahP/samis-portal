import { BrowserRouter as Router, Route } from 'react-router-dom';

import { ThemeProvider, createTheme } from '@mui/material/styles';
import { makeStyles } from '@mui/styles';
import CssBaseline from '@mui/material/CssBaseline';

import SideNav from './components/SideNav'
import './App.css';

import DashboardScreen from './screens/DashboardScreen'
import StudentCreateScreen from './students/StudentCreateScreen';
import StudentListScreen from './students/StudentListScreen';
import PageContainer from './components/PageContainer';
import Header from './components/Header';
import StreamListScreen from './streams/StreamListScreen';
import TestimonialListScreen from './testimonials/TestimonialListScreen';
import KcseSubjectListScreen from './kcseSubjects/KcseSubjectListScreen';
import AcademicSubjectListScreen from './academicSubjects/AcademicSubjectListScreen';
import ContactInfoListScreen from './contactInfo/ContactInfoListScreen';
import SchoolHistoryListScreen from './schoolHistory/SchoolHistoryListScreen';
import ArticleListScreen from './articles/ArticleListScreen';
// import InternalServerError from './components/errorPages/InternalServerError/InternalServerError';




const theme = createTheme({
    palette: {
      primary: {
        main: "#333996",
        light: '#3c44b126'
      },
      secondary: {
        main: "#f83245",
        light: '#f8324526'
      },
      background: {
        default: "#f4f5fd"
      },
    },
    overrides:{
      MuiAppBar:{
        root:{
          transform:'translateZ(0)',
          position: 'fixed'
        }
      },
    },
    props:{
      MuiIconButton:{
        disableRipple:true
      }
    }
  })


function App() {
    return (
        <ThemeProvider theme={theme}>
            <Router>
            <SideNav />
            <Header />
            <PageContainer>
                <Route path='/dashboard' component={DashboardScreen} />
                <Route path='/addStudents' component={StudentCreateScreen} />
                <Route path='/students' component={StudentListScreen} />
                <Route path='/streams' component={StreamListScreen} />
                <Route path='/testimonials' component={TestimonialListScreen} />
                <Route path='/kcse_subjects' component={KcseSubjectListScreen} />
                <Route path='/academic_subjects' component={AcademicSubjectListScreen} />
                <Route path='/contact_info' component={ContactInfoListScreen} />
                <Route path='/school_history' component={SchoolHistoryListScreen} />
                <Route path='/articles' component={ArticleListScreen} />
           
        
            </PageContainer>
        </Router>
        </ThemeProvider>
        
    );
}

export default App;
