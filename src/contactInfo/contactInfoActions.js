import axios from 'axios';
import { 
    CONTACT_INFO_GET_REQUEST,
    CONTACT_INFO_GET_SUCCESS,
    CONTACT_INFO_GET_FAIL,

    CONTACT_INFO_CREATE_REQUEST,
    CONTACT_INFO_CREATE_SUCCESS,
    CONTACT_INFO_CREATE_FAIL,
    CONTACT_INFO_CREATE_RESET,

    CONTACT_INFO_DETAILS_REQUEST,
    CONTACT_INFO_DETAILS_SUCCESS,
    CONTACT_INFO_DETAILS_FAIL,

    CONTACT_INFO_UPDATE_REQUEST,
    CONTACT_INFO_UPDATE_SUCCESS,
    CONTACT_INFO_UPDATE_FAIL,

    CONTACT_INFO_DELETE_REQUEST,
    CONTACT_INFO_DELETE_SUCCESS,
    CONTACT_INFO_DELETE_FAIL,



} from './contactInfoConstants';


export const getContactInfo = () => async (dispatch) => {
    try {
        dispatch({
            type: CONTACT_INFO_GET_REQUEST
        })
        const { data } = await axios.get(`/contact-info/v1`)
        dispatch({
            type: CONTACT_INFO_GET_SUCCESS,
            payload: data
        })

    } catch(error) {
        dispatch({
            type: CONTACT_INFO_GET_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })

    }
}



export const createContactInfo = (testimonial) => async (dispatch, getState) => {
    try {
        dispatch({
            type: CONTACT_INFO_CREATE_REQUEST
        })
        
        const { data } = await axios.post(
            `/contact-info/v1`,
            testimonial,
        )

        dispatch({
            type: CONTACT_INFO_CREATE_SUCCESS,
            success: true,
            payload: data
        })

    } catch(error) {
        dispatch({
            type: CONTACT_INFO_CREATE_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })

    }
}


export const testimonialDetails = () => async (dispatch) => {
    try {
        dispatch({ type: CONTACT_INFO_DETAILS_REQUEST })
        const { data } = await axios.get(`/contact-info/v1`) //proxy in package.json "http://127.0.0.1:8000/"

        dispatch({
            type:CONTACT_INFO_DETAILS_SUCCESS,
            payload: data
        })
    } catch (error) {
        dispatch({
            type:CONTACT_INFO_DETAILS_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })
    }
}


export const updateContactInfo = (testimonial) => async (dispatch) => {
    try {
        dispatch({
            type: CONTACT_INFO_UPDATE_REQUEST
        })
        
        const { data } = await axios.put(
            `/contact-info/v1/`,
            testimonial,
        )

        dispatch({
            type: CONTACT_INFO_UPDATE_SUCCESS,
            payload: data
        })

        //update details
        dispatch({
            type: CONTACT_INFO_DETAILS_SUCCESS,
            payload: data
        })

    } catch(error) {
        dispatch({
            type: CONTACT_INFO_UPDATE_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })

    }
}


export const deleteContactInfo = () => async (dispatch, getState) => {
    try {
        dispatch({
            type: CONTACT_INFO_DELETE_REQUEST
        })
        
        const { data } = await axios.delete(
            `/contact-info/v1/`,
        )

        dispatch({
            type: CONTACT_INFO_DELETE_SUCCESS,
        })

    } catch(error) {
        dispatch({
            type: CONTACT_INFO_DELETE_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })

    }
}


export const uploadContactInfoImage = () => async (dispatch) => {
    try {
        dispatch({ type: CONTACT_INFO_GET_REQUEST })
        const { data } = await axios.get(`/contact-info/v1/images/upload`)

        dispatch({
            type:CONTACT_INFO_GET_SUCCESS,
            payload: data
        })
    } catch (error) {
        dispatch({
            type:CONTACT_INFO_GET_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })
    }
}