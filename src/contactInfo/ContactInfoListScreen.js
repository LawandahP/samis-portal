import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import Loader from '../components/Loader'
import ToastAlert from '../components/ToastAlert'
import TableComponent from '../components/TableComponent'

import { createContactInfo, deleteContactInfo, getContactInfo, updateContactInfo } from './contactInfoActions'
import { makeStyles } from '@mui/styles'

import EditOutlinedIcon from '@mui/icons-material/EditOutlined';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import Paper from '@mui/material/Paper';
import TableRow from '@mui/material/TableRow';
import Search from '@mui/icons-material/Search'
import Toolbar from '@mui/material/Toolbar';
import InputAdornment from '@mui/material/InputAdornment';
import Fab from '@mui/material/Fab';
import AddIcon from '@mui/icons-material/Add';
import Controls from '../components/controls/Controls'
import ContactInfoForm from './ContactInfoForm'
import Popup from '../components/Popup'
import ConfirmDialog from '../components/ConfirmDialog'
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';

const useStyles = makeStyles(theme => ({
    searchInput: {
        width : "85%",
    },
    newButton: {
        position: "absolute",
        right: "10px",
        margin: "1rem"
    },
    // width: '100%', overflow: 'hidden', marginTop: "1rem" 
    paper: {
        width: "100%",
        overflow: "hidden",
        marginTop: "1rem",
        marginBottom: "2rem"
    }
}))
const headCells = [
    { id: 'physical_address', label: 'Physical Address', minWidth: 170 },
    { id: 'location', label: 'Location', minWidth: 200 },
    { id: 'postal_address', label: 'Postal Address', minWidth: 170 },
    { id: 'country_code', label: 'Country Code',  minWidth: 170  },
    { id: 'primary_phone_no', label: 'Phone No.',  minWidth: 170  },
    { id: 'other_phone_nos', label: 'Other Phone Nos',  minWidth: 170  },
    { id: 'email', label: 'Email',  minWidth: 170  },
    { id: 'other_emails', label: 'Other Emails',  minWidth: 170  },
    { id: 'actions', label: 'Actions', disableSorting: true },

]



function ContactInfoListScreen() {
    const dispatch = useDispatch();
    const classes = useStyles();

    const [ search, setSearch ] = useState({fn:items => {return items;}})
    const [ recordForEdit, setRecordForEdit ] = useState(null);
    const [ openPopup, setOpenPopup ] = useState(false)

    const contactInfoGet = useSelector(state => state.contactInfoGet)
    const { loading, error, contactInfo } = contactInfoGet

    const contactInfoCreate = useSelector(state => state.contactInfoCreate)
    const { success: successCreate } = contactInfoCreate

    const [ confirmDialog, setConfirmDialog ] = useState({isOpen: false, title: '', subTitle: ''})

    const { TblContainer, 
            TblHead, 
            TblPagination, 
        } = TableComponent (contactInfo, headCells)
    
    useEffect(() => {
        dispatch(getContactInfo())
        
    }, [dispatch])

    const isObject = (val) => {
        if (val === null) 
            return false;
        return (typeof val === "object");
    }

    const objProps = (obj) => {
        for (let val in obj) {
            if (isObject(obj[val])) {
                objProps(obj[val])
            } else {
            console.log(val, obj[val])
        }}
    };

    objProps(contactInfo);

    // for (let val in contactInfo) {
    //     if (isObject(contactInfo[val])) {
    //         for (let val2 in contactInfo[val]) {
    //             console.log(val2, contactInfo[val][val2])
    //         }
    //     } else {
    //     console.log(val, contactInfo[val])
    // }}

    


    const addOrEdit = (contactInfo, handleResetForm) => {
        // if (contactInfo)
        //     dispatch(createContactInfo(contactInfo))
        if (contactInfo)
            dispatch(updateContactInfo(contactInfo))
        handleResetForm()
        setOpenPopup(false);
        dispatch(getContactInfo())
        
           
        // setNotify({
        //     isOpen:true,
        //     message:'Submitted Successfully',
        //     type:'success'
        // })         
    }

    const editHandler = (contactInfo) => { 
        setRecordForEdit(contactInfo)
        setOpenPopup(true)
    }

    const deleteHandler = () => {
        setConfirmDialog({
            ...confirmDialog,
            isOpen: false
        })
        dispatch(deleteContactInfo())  
        dispatch(getContactInfo())
    }


    return (
        <div>    
            <Toolbar>   
                <Fab color="primary" size="small" sx={{align: 'right' }} aria-label="add" 
                    onClick = {() => setOpenPopup(true)}
                    >
                    <AddIcon 
                        type="button"/>
                </Fab>
            </Toolbar>              
            
                <Paper className={classes.paper}>
                    
                    <TblContainer>
                        <TblHead />

                        <TableBody> 
                        { loading
                            ? <Loader />
                            : error
                            ? <ToastAlert bg="danger">{error}</ToastAlert>
                            : !contactInfo ? "No Contact Info"
                            : (                      
                            <TableRow>
                            
                                <TableCell>{contactInfo.physical_address ? contactInfo.physical_address : "Not found"}</TableCell>
                                <TableCell>
                                    <ul>
                                        { contactInfo.location ? 
                                            Object.entries(contactInfo.location).map(([key, value]) => (
                                                <List>
                                                    <ListItem disablePadding>
                                                        {key}: {value ? value : "not set"}
                                                    </ListItem>
                                                </List>
                                        ))
                                    : "-"}
                                    </ul>
                                </TableCell>
                                <TableCell>{contactInfo.postal_address ? contactInfo.postal_address : "-"}</TableCell>
                                <TableCell>{contactInfo.country_code}</TableCell>
                                <TableCell>{contactInfo.primary_phone_no}</TableCell>
                                <TableCell>
                                    <ul>
                                        { contactInfo.other_phone_nos ? contactInfo.other_phone_nos.map(phone_no => (
                                            <List>
                                                <ListItem disablePadding>
                                                    {phone_no}
                                                </ListItem>
                                            </List>
                                        )) : "not set"}
                                    </ul>
                                </TableCell>
                                <TableCell>{contactInfo.email}</TableCell>
                                <TableCell>
                                    <ul>
                                        { contactInfo.other_emails && contactInfo.other_emails.map(other_emails => (
                                            <List>
                                                <ListItem disablePadding>
                                                    {other_emails}
                                                </ListItem>
                                            </List>
                                        ))}
                                    </ul>
                                </TableCell>
                                <TableCell>
                                    <Controls.ActionButton
                                        color="primary">
                                        <EditOutlinedIcon 
                                            onClick={() => editHandler(contactInfo)}
                                            fontSize="small" />
                                    </Controls.ActionButton>
                                    <Controls.ActionButton 
                                        color="secondary"
                                        onClick={() => {
                                            setConfirmDialog({
                                                isOpen: true,
                                                title: "Are you sure you want to delete this contactInfo?",
                                                subTitle: "You can't undo this operation",
                                                onConfirm: () => { deleteHandler()  }
                                            })
                                            
                                        }}>
                                        <DeleteOutlineIcon 
                                            fontSize="small" />
                                    </Controls.ActionButton>
                                </TableCell>
                                                          
                            </TableRow>
                        )} 
                        </TableBody>
                    </TblContainer>
                </Paper>
                 
            
            <Popup
                openPopup = {openPopup}
                setOpenPopup = {setOpenPopup}
                title="Create ContactInfos"
            >
                <ContactInfoForm 
                    recordForEdit = {recordForEdit}
                    addOrEdit = {addOrEdit}/>                
            </Popup>  
            <ConfirmDialog 
                confirmDialog={confirmDialog}
                setConfirmDialog={setConfirmDialog} />
            
            </div>
    )
}

export default ContactInfoListScreen
