import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import Loader from '../components/Loader'
import ToastAlert from '../components/ToastAlert'
import TableComponent from '../components/TableComponent'

import { createTestimonial, deleteTestimonial, listTestimonials, updateTestimonial } from './testimonialActions'
import { makeStyles } from '@mui/styles'

import EditOutlinedIcon from '@mui/icons-material/EditOutlined';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import Paper from '@mui/material/Paper';
import TableRow from '@mui/material/TableRow';
import Search from '@mui/icons-material/Search'
import Toolbar from '@mui/material/Toolbar';
import InputAdornment from '@mui/material/InputAdornment';
import Fab from '@mui/material/Fab';
import AddIcon from '@mui/icons-material/Add';
import Controls from '../components/controls/Controls'
import TestimonialForm from './TestimonialForm'
import Popup from '../components/Popup'
import ConfirmDialog from '../components/ConfirmDialog'

const useStyles = makeStyles(theme => ({
    searchInput: {
        width : "85%",
    },
    newButton: {
        position: "absolute",
        right: "10px",
        margin: "1rem"
    },
    paper: {
        width: "100%",
        overflow: "hidden",
        marginTop: "1rem",
        marginBottom: "2.5rem"
    }
}))
const headCells = [
    { id: 'name', label: 'Testimonial Name', minWidth: 170 },
    { id: 'role', label: 'Role', minWidth: 170 },
    { id: 'info', label: 'info', minWidth: 370, disableSorting: true },
    { id: 'image_name', label: 'Image', disableSorting: true},
    { id: 'actions', label: 'Actions', disableSorting: true },

]


function TestimonialListScreen() {
    const dispatch = useDispatch();
    const classes = useStyles();

    const [ search, setSearch ] = useState({fn:items => {return items;}})
    const [ recordForEdit, setRecordForEdit ] = useState(null);
    const [ openPopup, setOpenPopup ] = useState(false)

    const testimonialList = useSelector(state => state.testimonialList)
    const { loading, error, testimonials } = testimonialList

    const testimonialCreate = useSelector(state => state.testimonialCreate)
    const { success: successCreate } = testimonialCreate

    const [ confirmDialog, setConfirmDialog ] = useState({isOpen: false, title: '', subTitle: ''})

    const { TblContainer, 
            TblHead, 
            TblPagination, 
            recordsAfterPagingAndSorting 
        } = TableComponent (testimonials, headCells, search)
    
    useEffect(() => {
        dispatch(listTestimonials())
    }, [dispatch])

    const handleSearch = (e) => {
        e.preventDefault()
        let target = e.target
        setSearch({
            fn:items => {
                if(target.value === "")
                    return items;
                else
                    return items.filter(x => x.name.toLowerCase().includes(target.value))
            }
        })
    }

    const addOrEdit = (testimonial, handleResetForm) => {
        if (testimonial.id === 0) {
            dispatch(createTestimonial(testimonial))
            dispatch(listTestimonials())
        } else {
            dispatch(updateTestimonial(testimonial))
            dispatch(listTestimonials())
        }
        handleResetForm()
        setOpenPopup(false);
    
        
           
        // setNotify({
        //     isOpen:true,
        //     message:'Submitted Successfully',
        //     type:'success'
        // })         
    }

    const editHandler = (testimonial) => { 
        setRecordForEdit(testimonial)
        setOpenPopup(true)
    }

    const deleteHandler = (id) => {
        setConfirmDialog({
            ...confirmDialog,
            isOpen: false
        })
        dispatch(deleteTestimonial(id))  
        dispatch(listTestimonials())
    }

    return (
        <div>                   
            { loading
            ? <Loader />
            : error
            ? <ToastAlert bg="danger">{error}</ToastAlert>
            : (
                <Paper className={classes.paper}>
                    <Toolbar>
                        <Controls.Input 
                            className={classes.searchInput}
                            label="Search Testimonial"
                            size="small"
                            InputProps = {{
                                startAdornment:(
                                    <InputAdornment position="start">
                                        <Search/>
                                    </InputAdornment>
                                )
                            }}  
                            onChange = { handleSearch } 
                        />

                    <Fab color="primary" size="small" sx={{marginLeft: '.5rem' }} aria-label="add" 
                        onClick = {() => setOpenPopup(true)}
                        >
                        <AddIcon 
                            type="button"/>
                    </Fab>

                   
                    </Toolbar>
                    <TblContainer>
                        <TblHead />

                        <TableBody>
                        {
                            recordsAfterPagingAndSorting() && recordsAfterPagingAndSorting().map(testimonial => 
                                (<TableRow key={testimonial.id}>
                                    <TableCell>{testimonial.name}</TableCell>
                                    <TableCell>{testimonial.role}</TableCell>
                                    <TableCell>{testimonial.info}</TableCell>
                                    <TableCell>{testimonial.image_name}</TableCell>
                                    <TableCell>
                                        <Controls.ActionButton
                                            color="primary">
                                            <EditOutlinedIcon 
                                                onClick={() => editHandler(testimonial)}
                                                fontSize="small" />
                                        </Controls.ActionButton>
                                        <Controls.ActionButton 
                                            color="secondary"
                                            onClick={() => {
                                                setConfirmDialog({
                                                    isOpen: true,
                                                    title: "Are you sure you want to delete this testimonial?",
                                                    subTitle: "You can't undo this operation",
                                                    onConfirm: () => { deleteHandler(testimonial.id)  }
                                                })
                                                
                                            }}>
                                            <DeleteOutlineIcon 
                                                fontSize="small" />
                                            </Controls.ActionButton>
                                        </TableCell>
                                                                
                                </TableRow>)
                        )}
                        </TableBody>
                    </TblContainer>
                    <TblPagination />
                </Paper>
                 
            )}
            <Popup
                openPopup = {openPopup}
                setOpenPopup = {setOpenPopup}
                title="Create Testimonials"
            >
                <TestimonialForm 
                    recordForEdit = {recordForEdit}
                    addOrEdit = {addOrEdit}/>                
            </Popup>  
            <ConfirmDialog 
                confirmDialog={confirmDialog}
                setConfirmDialog={setConfirmDialog} />
            
            </div>
    )
}

export default TestimonialListScreen
