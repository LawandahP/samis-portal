import React, {useEffect} from 'react'

import { useDispatch, useSelector } from 'react-redux';

import Loader from '../components/Loader';
import ToastAlert from '../components/ToastAlert';
import {useForm, Form } from '../components/useForm';
import Controls from '../components/controls/Controls';
import IconButton from '@mui/material/IconButton';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import Paper  from '@mui/material/Paper';

import PhotoCamera from '@mui/icons-material/PhotoCamera';

import { styled } from '@mui/material/styles';
import { makeStyles } from '@mui/styles';
import DropzoneComponent from '../components/FileUpload';
import { Avatar } from '@mui/material';
import { Image } from 'react-bootstrap';


const Input = styled('input')({
    display: 'none',
  });

const useStyles = makeStyles(theme => ({
    paper: {
        width: "100%",
        overflow: "hidden",
        marginTop: "1rem",
        textAlign: "center",
        padding: theme.spacing(6),
        marginBottom: "1rem"
    },
    input: {
        marginRight: "5px"
    }
}))

const defaultImageSrc = "/img/placeholder.jpg"


function TestimonialForm(props, {history}) {
    const classes = useStyles();
    const { addOrEdit, recordForEdit, testimonialId } = props;
    const dispatch = useDispatch();

    const streamCreate = useSelector(state => state.streamCreate)
    const { loading: loadingCreate, error: errorCreate, success: successCreate } = streamCreate

    // const [successMessage, setSuccessMessage] = useState('')
    
    const validate = (fieldValues = values) => {
        let temp = {...errors}
        if('name' in fieldValues)
            temp.name = fieldValues.name ? "" : "Testimonial Giver Name is Required"
        if('role' in fieldValues)
            temp.role = fieldValues.role ? "" : "Role is Required"
        if('info' in fieldValues)
            temp.role = fieldValues.info ? "" : "Info is Required"
        // if('image_src' in fieldValues)
        //     temp.image_src = fieldValues.image_src === defaultImageSrc ? "" : "No image chosen"
        // temp.phone_no = (\\+?\\d{9,13}).test(values.phone_no) ? "" : "Student Name is Required"
        setErrors({ ...temp })

        // tests whether post array elements passes text implemented by validate() function
        if (fieldValues === values)
            return Object.values(temp).every(x => x === "")
        
    }

    const initialFValues = {
        id: 0,
        name: '',
        role: '',
        info: '',
        image_name: '',
        image_src: defaultImageSrc,
        image_file: '',

    }

    const { values,  
            setValues,
            errors, 
            setErrors, 
            handleResetForm, 
            handleInputChange } = useForm(initialFValues, true, validate);

    
    
    useEffect(() => {
        if(recordForEdit != null)
            setValues({
                ...recordForEdit
            })
    
    }, [dispatch, history, recordForEdit, successCreate, loadingCreate, errorCreate, setValues, testimonialId ])


    const submitHandler = (e) => {
        e.preventDefault()
        if (validate()) {
            // const formData = new FormData()
            // formData.append('name', values.name)
            // formData.append('role', values.role)
            // formData.append('info', values.info)
            // formData.append('image_name', values.image_name)
            // formData.append('image_file', values.image_file)
            addOrEdit(values, handleResetForm);
            handleResetForm()
        }
        // name: '',
        // role: '',
        // info: '',
        // image_name: '',
        // image_src: defaultImageSrc,
        // image_file: '',
           

    // const submitHandler = (e) => {
    //     e.preventDefault()
    //     if (validate()) 
    //         dispatch(registerTenant(
    //             values
    //         ))
    //         handleResetForm()
    }

   
    return (
        
        <div>
            { loadingCreate && <Loader />}
            { errorCreate && <ToastAlert bg="danger">{errorCreate}</ToastAlert>}
                <Form onSubmit={submitHandler}>
                    <Grid container>
                        <Grid item md={6} xs={12}>
                            <Controls.Input
                                error={errors.name}
                                label="Name"
                                value={values.name}
                                name='name'
                                onChange={handleInputChange}
                            />
                        </Grid>

                        <Grid item md={6} xs={12}>
                            <Controls.Input
                                error={errors.role}
                                label="Role"
                                value={values.role}
                                name='role'
                                onChange={handleInputChange}
                            />
                        </Grid>

                        <Grid item md={6} xs={12}>
                            <Controls.Input 
                                multiline="true"
                                error={errors.info}
                                label="Info"
                                name='info'
                                value={values.info}
                                onChange={handleInputChange}
                            />
                        </Grid>

                        <Grid item md={6} xs={12}>
                            <Controls.Input
                                // error={errors.image}
                                label="Image Name"
                                name='image_name'
                                value={values.image_name}
                                onChange={handleInputChange}
                            />
                        </Grid>

                        <Paper className={classes.paper}>
                            
                            <label htmlFor="contained-button-file">
                                <Input accept="image/*" 
                                    // error={errors.image_src}
                                    id="contained-button-file" 
                                    type="file" 
                                    onChange={handleInputChange}/>
                                    <Button className={classes.input} variant="contained" component="span">
                                        <PhotoCamera />  Upload image
                                    </Button>
                            </label>
                        
                            <Image src={values.image_src} style={{width: "200px", height: "200px", textAlign: "center"}} className="ml-3" alt="upload" fluid />
                        
                        </Paper>
                        

                        <div>
                            
                                <Controls.Button
                                    type="submit" 
                                    text="Submit"
                                />
                                <Controls.Button 
                                    onClick={handleResetForm}
                                    text="Reset"
                                    color="secondary"
                                />
                        
                        </div>
                        

                    </Grid>
                </Form>
        </div>
        
            
            
                       
    )
}

export default TestimonialForm
