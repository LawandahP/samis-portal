import axios from 'axios';
import { 
    TESTIMONIAL_GET_REQUEST,
    TESTIMONIAL_GET_SUCCESS,
    TESTIMONIAL_GET_FAIL,

    TESTIMONIAL_CREATE_REQUEST,
    TESTIMONIAL_CREATE_SUCCESS,
    TESTIMONIAL_CREATE_FAIL,
    TESTIMONIAL_CREATE_RESET,

    TESTIMONIAL_DETAILS_REQUEST,
    TESTIMONIAL_DETAILS_SUCCESS,
    TESTIMONIAL_DETAILS_FAIL,

    TESTIMONIAL_UPDATE_REQUEST,
    TESTIMONIAL_UPDATE_SUCCESS,
    TESTIMONIAL_UPDATE_FAIL,

    TESTIMONIAL_DELETE_REQUEST,
    TESTIMONIAL_DELETE_SUCCESS,
    TESTIMONIAL_DELETE_FAIL,



} from './testimonialConstants';


export const listTestimonials = () => async (dispatch) => {
    try {
        dispatch({ type: TESTIMONIAL_GET_REQUEST })
        const { data } = await axios.get(`/testimonials/v1`)

        dispatch({
            type:TESTIMONIAL_GET_SUCCESS,
            payload: data
        })
    } catch (error) {
        dispatch({
            type:TESTIMONIAL_GET_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })
    }
}



export const createTestimonial = (testimonial) => async (dispatch, getState) => {
    try {
        dispatch({
            type: TESTIMONIAL_CREATE_REQUEST
        })
        
        const { data } = await axios.post(
            `/testimonials/v1`,
            testimonial,
        )

        dispatch({
            type: TESTIMONIAL_CREATE_SUCCESS,
            success: true,
            payload: data
        })

    } catch(error) {
        dispatch({
            type: TESTIMONIAL_CREATE_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })

    }
}


export const testimonialDetails = (id) => async (dispatch) => {
    try {
        dispatch({ type: TESTIMONIAL_DETAILS_REQUEST })
        const { data } = await axios.get(`/testimonials/${id}`) //proxy in package.json "http://127.0.0.1:8000/"

        dispatch({
            type:TESTIMONIAL_DETAILS_SUCCESS,
            payload: data
        })
    } catch (error) {
        dispatch({
            type:TESTIMONIAL_DETAILS_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })
    }
}


export const updateTestimonial = (testimonial) => async (dispatch) => {
    try {
        dispatch({
            type: TESTIMONIAL_UPDATE_REQUEST
        })
        
        const { data } = await axios.put(
            `/testimonials/v1/${testimonial.id}`,
            testimonial,
        )

        dispatch({
            type: TESTIMONIAL_UPDATE_SUCCESS,
            payload: data
        })

        //update details
        dispatch({
            type: TESTIMONIAL_DETAILS_SUCCESS,
            payload: data
        })

    } catch(error) {
        dispatch({
            type: TESTIMONIAL_UPDATE_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })

    }
}


export const deleteTestimonial = (id) => async (dispatch, getState) => {
    try {
        dispatch({
            type: TESTIMONIAL_DELETE_REQUEST
        })
        
        const { data } = await axios.delete(
            `/testimonials/v1/${id}`,
            //config
        )

        dispatch({
            type: TESTIMONIAL_DELETE_SUCCESS,
        })

    } catch(error) {
        dispatch({
            type: TESTIMONIAL_DELETE_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })

    }
}


export const uploadTestimonialImage = () => async (dispatch) => {
    try {
        dispatch({ type: TESTIMONIAL_GET_REQUEST })
        const { data } = await axios.get(`/testimonials/v1/images/upload`)

        dispatch({
            type:TESTIMONIAL_GET_SUCCESS,
            payload: data
        })
    } catch (error) {
        dispatch({
            type:TESTIMONIAL_GET_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })
    }
}